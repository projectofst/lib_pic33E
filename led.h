#ifndef _LED_H
#define _LED_H

//to do: implement way to receive pins as argument to config function
#define LAT_RGB_R   LATEbits.LATE5
#define LAT_RGB_G   LATEbits.LATE3
#define LAT_RGB_B   LATEbits.LATE7

#define TRIS_RGB_R  TRISEbits.TRISE5  
#define TRIS_RGB_G  TRISEbits.TRISE3
#define TRIS_RGB_B  TRISEbits.TRISE7

void config_rgb (void);
void rgb_clear  (void);
void rgb_on     (char color);

#endif