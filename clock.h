void test_clock(void)
{
	// Configure RB12 as output
	TRISBbits.TRISB12 = 0;
	// Generate a square wave at 1/16 of FCY.
	asm("loop:");
	asm("btg LATB, #12"); // 2 cycles
	asm("nop"); // 1 cycle
	asm("nop"); // 1 cycle
	asm("bra loop"); // 4 cycles
}
