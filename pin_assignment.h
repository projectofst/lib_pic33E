#ifndef _LIB_PIC33E_PIN_ASIGNMENT_H_
#define _LIB_PIC33E_PIN_ASIGNMENT_H_

/*
 * To perform PPS operations you need to:
 *
 * Unlock the PPS registers with 'PPSUnlock'
 * Assign the peripherals Output functions to the wanted output pin.
 * Assign the wanted input pins to the peripheral's input functions.
 *
 * Example with can module:
 *
    PPSUnLock;
	PPSOutput(OUT_FN_PPS_C1TX, OUT_PIN_PPS_RP118);
	PPSInput(IN_FN_PPS_C1RX, IN_PIN_PPS_RPI119);
	PPSOutput(OUT_FN_PPS_C2TX, OUT_PIN_PPS_RP120);
	PPSInput(IN_FN_PPS_C2RX, IN_PIN_PPS_RPI121);
	PPSLock;
 *
 *
 * To pass the pins to the configuration function you should have uint16_t
 * members that then take the values defined in the pps.h file. Like
 * OUT_PIN_PPS_RP118 to use RP118 as an output pin and IN_PIN_PSS_RPI119 to use
 * the pin RPI119 as an input pin. The references like RP118 and RP119 are
 * written in the PIC schematic symbol and the datasheet pin diagrams.
 */

/*
 * Makes some configuration related to pin assignment and PPS that should be made
 * for everyone.
 *
 * It makes all ANSELx register '0' so if an analog module is used the ANSEL
 * bits corresponding to the used pins should be set.
 */
void __attribute__((user_init)) config_pin_assignment(void);

#endif
