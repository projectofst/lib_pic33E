#ifndef NOSPI

#include <xc.h>
#include <stdlib.h>

#include "lib_pic33e/timing.h"
#include "SPI.h"

inline __attribute__((__always_inline__)) SPI_TRANSFER_MODE SPI1_TransferModeGet(void);
inline __attribute__((__always_inline__)) SPI_TRANSFER_MODE SPI2_TransferModeGet(void);
inline __attribute__((__always_inline__)) SPI_TRANSFER_MODE SPI3_TransferModeGet(void);
inline __attribute__((__always_inline__)) SPI_TRANSFER_MODE SPI4_TransferModeGet(void);

/**********************************************************************
 * Name:    get_prescales
 * Args:    desired baudrate, *primary prescale, *secondary prescale
 * Return:  best baudrate you can get for the defined FCY
 * Desc:    calculates the prescales that will get you the closest baudrate
 **********************************************************************/
uint32_t get_prescales(uint32_t baudrate, uint8_t *p_presc, uint8_t *s_presc){

    int i;
    int j;
    float error = 100;
    float best_prescaler_error = 100;
    const uint8_t prim[4] = {64,16,4,1};
    const uint8_t sec[8] = {8,7,6,5,4,3,2,1};
    uint32_t fsck;
    uint8_t best_prescaler[2] = {0,0};

    for(i=0; i<4; i++){
        for(j=0; j<8; j++){
            fsck = FCY/(prim[i]*sec[j]);
            error = (abs((int)fsck-(int)baudrate)*100.0/baudrate);
            if(error < best_prescaler_error){
                best_prescaler[0] = i;
                best_prescaler[1] = j;
                best_prescaler_error = error;
            }
            
        }
    }
    *p_presc = best_prescaler[0];
    *s_presc = best_prescaler[1];
	fsck = FCY/(prim[best_prescaler[0]]*sec[best_prescaler[1]]);


    return fsck;

}

/**********************************************************************
 * Name:    config_SPI1
 * Args:    SPI_Peripheral_Configuration config - configuration struct
 * Return:  best baudrate you can get for the defined FCY
 * Desc:    config SPI module
 **********************************************************************/
uint32_t config_SPI1(SPI_Peripheral_Configuration config){

    uint32_t baudrate;
    uint8_t p_presc;
    uint8_t s_presc;

    SPI1STATbits.SPIEN = 0; //Assure module is disabled

    /* SPI1CON1 */
    SPI1CON1bits.DISSCK = config.disable_SCK_pin;
    SPI1CON1bits.DISSDO = config.disable_SDO_pin;
    SPI1CON1bits.MODE16 = config.transfer_mode;
    SPI1CON1bits.SSEN = config.enable_SS_pin;
    SPI1CON1bits.MSTEN = config.enable_master_mode;
    /* Timing configuration */
    SPI1CON1bits.CKP = ((config.SPI_mode >> 1) & 0b1);
    SPI1CON1bits.CKE = (~(config.SPI_mode & 0b1));
    SPI1CON1bits.SMP = config.data_sample_phase;

    /* Chose baudrate prescalers */
    baudrate = get_prescales(config.baudrate, &p_presc, &s_presc);
    SPI1CON1bits.PPRE = p_presc;
    SPI1CON1bits.SPRE = s_presc;

    /* SPI1CON2 */
    SPI1CON2bits.FRMEN = 0;  // Disable all framed SPI support
    SPI1CON2bits.SPIBEN = 1; // Enhanced mode is enabled

    /* SPI1STAT */
    SPI1STATbits.SPISIDL = 0; // continue operation in Idle
    SPI1STATbits.SPIROV = 0; // Clear overflow
    SPI1STATbits.SISEL = 0b011; // Set interrupt to SPI receive buffer full is set (Not used)

    /* Configuration is finished. Enable the module */
    SPI1STATbits.SPIEN = config.enabled;
    return baudrate;
}

/**********************************************************************
 * Name:    config_SPI2
 * Args:    SPI_Peripheral_Configuration config - configuration struct
 * Return:  best baudrate you can get for the defined FCY
 * Desc:    config SPI module
 **********************************************************************/
uint32_t config_SPI2(SPI_Peripheral_Configuration config){
    
    uint32_t baudrate;
    uint8_t p_presc;
    uint8_t s_presc;

    SPI2STATbits.SPIEN = 0; //Assure module is disabled

    /* SPI2CON1 */
    SPI2CON1bits.DISSCK = config.disable_SCK_pin;
    SPI2CON1bits.DISSDO = config.disable_SDO_pin;
    SPI2CON1bits.MODE16 = config.transfer_mode;
    SPI2CON1bits.SSEN = config.enable_SS_pin;
    SPI2CON1bits.MSTEN = config.enable_master_mode;
    /* Timing configuration */
    SPI2CON1bits.CKP = ((config.SPI_mode >> 1) & 0b1);
    SPI2CON1bits.CKE = (~(config.SPI_mode & 0b1));
    SPI2CON1bits.SMP = config.data_sample_phase;

    /* Chose baudrate prescalers */
    baudrate = get_prescales(config.baudrate, &p_presc, &s_presc);
    SPI2CON1bits.PPRE = p_presc;
    SPI2CON1bits.SPRE = s_presc;

    /* SPI2CON2 */
    SPI2CON2bits.FRMEN = 0;  // Disable all framed SPI support
    SPI2CON2bits.SPIBEN = 1; // Enhanced mode is enabled

    /* SPI2STAT */
    SPI2STATbits.SPISIDL = 0; // continue operation in Idle
    SPI2STATbits.SPIROV = 0; // Clear overflow
    SPI2STATbits.SISEL = 0b011; // Set interrupt to SPI receive buffer full is set (Not used)

    /* Configuration is finished. Enable the module */
    SPI2STATbits.SPIEN = config.enabled;
    return baudrate;
}

/**********************************************************************
 * Name:    config_SPI3
 * Args:    SPI_Peripheral_Configuration config - configuration struct
 * Return:  best baudrate you can get for the defined FCY
 * Desc:    config SPI module
 **********************************************************************/
uint32_t config_SPI3(SPI_Peripheral_Configuration config){
    
    uint32_t baudrate;
    uint8_t p_presc;
    uint8_t s_presc;

    SPI1STATbits.SPIEN = 0; //Assure module is disabled

    /* SPI3CON1 */
    SPI3CON1bits.DISSCK = config.disable_SCK_pin;
    SPI3CON1bits.DISSDO = config.disable_SDO_pin;
    SPI3CON1bits.MODE16 = config.transfer_mode;
    SPI3CON1bits.SSEN = config.enable_SS_pin;
    SPI3CON1bits.MSTEN = config.enable_master_mode;
    /* Timing configuration */
    SPI3CON1bits.CKP = ((config.SPI_mode >> 1) & 0b1);
    SPI3CON1bits.CKE = (~(config.SPI_mode & 0b1));
    SPI3CON1bits.SMP = config.data_sample_phase;

    /* Chose baudrate prescalers */
    baudrate = get_prescales(config.baudrate, &p_presc, &s_presc);
    SPI3CON1bits.PPRE = p_presc;
    SPI3CON1bits.SPRE = s_presc;

    /* SPI3CON2 */
    SPI3CON2bits.FRMEN = 0;  // Disable all framed SPI support
    SPI3CON2bits.SPIBEN = 1; // Enhanced mode is enabled

    /* SPI3STAT */
    SPI3STATbits.SPISIDL = 0; // continue operation in Idle
    SPI3STATbits.SPIROV = 0; // Clear overflow
    SPI3STATbits.SISEL = 0b011; // Set interrupt to SPI receive buffer full is set (Not used)

    /* Configuration is finished. Enable the module */
    SPI3STATbits.SPIEN = config.enabled;
    return baudrate;
}

/**********************************************************************
 * Name:    config_SPI4
 * Args:    SPI_Peripheral_Configuration config - configuration struct
 * Return:  best baudrate you can get for the defined FCY
 * Desc:    config SPI module
 **********************************************************************/
uint32_t config_SPI4(SPI_Peripheral_Configuration config){
    
    uint32_t baudrate;
    uint8_t p_presc;
    uint8_t s_presc;

    SPI4STATbits.SPIEN = 0; //Assure module is disabled

    /* SPI3CON1 */
    SPI4CON1bits.DISSCK = config.disable_SCK_pin;
    SPI4CON1bits.DISSDO = config.disable_SDO_pin;
    SPI4CON1bits.MODE16 = config.transfer_mode;
    SPI4CON1bits.SSEN = config.enable_SS_pin;
    SPI4CON1bits.MSTEN = config.enable_master_mode;
    /* Timing configuration */
    SPI4CON1bits.CKP = ((config.SPI_mode >> 1) & 0b1);
    SPI4CON1bits.CKE = (~(config.SPI_mode & 0b1));
    SPI4CON1bits.SMP = config.data_sample_phase;

    /* Chose baudrate prescalers */
    baudrate = get_prescales(config.baudrate, &p_presc, &s_presc);
    SPI4CON1bits.PPRE = p_presc;
    SPI4CON1bits.SPRE = s_presc;

    /* SPI3CON2 */
    SPI4CON2bits.FRMEN = 0;  // Disable all framed SPI support
    SPI4CON2bits.SPIBEN = 1; // Enhanced mode is enabled

    /* SPI3STAT */
    SPI4STATbits.SPISIDL = 0; // continue operation in Idle
    SPI4STATbits.SPIROV = 0; // Clear overflow
    SPI4STATbits.SISEL = 0b011; // Set interrupt to SPI receive buffer full is set (Not used)

    /* Configuration is finished. Enable the module */
    SPI4STATbits.SPIEN = config.enabled;
    return baudrate;
}

/**********************************************************************
 * Name:    SPI1_enable_module
 * Args:    -
 * Return:  -
 * Desc:    enables SPI1 module
 **********************************************************************/
void SPI1_enable_module(){
    SPI1STATbits.SPIEN = 1;
    return;
}

/**********************************************************************
 * Name:    SPI2_enable_module
 * Args:    -
 * Return:  -
 * Desc:    enables SPI2 module
 **********************************************************************/
void SPI2_enable_module(){
    SPI2STATbits.SPIEN = 1;
    return;
}

/**********************************************************************
 * Name:    SPI3_enable_module
 * Args:    -
 * Return:  -
 * Desc:    enables SPI3 module
 **********************************************************************/
void SPI3_enable_module(){
    SPI3STATbits.SPIEN = 1;
    return;
}

/**********************************************************************
 * Name:    SPI4_enable_module
 * Args:    -
 * Return:  -
 * Desc:    enables SPI4 module
 **********************************************************************/
void SPI4_enable_module(){
    SPI4STATbits.SPIEN = 1;
    return;
}

/**
 * @brief Exchange one element of data. Transmit or receive
 *
 * @param pTransmitData Data element to send. NULL when receiving
 * only.
 * @param pReceiveData  Data element to receive. NULL when
 * transmitting only.
 */
void SPI1_Exchange( uint8_t *pTransmitData, uint8_t *pReceiveData )
{

    while( SPI1STATbits.SPITBF == true )
    {

    }

    if (SPI1_TransferModeGet() == SPI_TRANSFER_MODE_16BIT)
        SPI1BUF = *((uint16_t*)pTransmitData);
    else
        SPI1BUF = *((uint8_t*)pTransmitData);

    while ( SPI1STATbits.SRXMPT == true);

    if (SPI1_TransferModeGet() == SPI_TRANSFER_MODE_16BIT)
        *((uint16_t*)pReceiveData) = SPI1BUF;
    else
        *((uint8_t*)pReceiveData) = SPI1BUF;

    return;
}

/**
 * @brief Exchange one element of data. Transmit or receive
 *
 * @param pTransmitData Data element to send. NULL when receiving
 * only.
 * @param pReceiveData  Data element to receive. NULL when
 * transmitting only.
 */
void SPI2_Exchange( uint8_t *pTransmitData, uint8_t *pReceiveData )
{

    while( SPI2STATbits.SPITBF == true )
    {

    }

    if (SPI2_TransferModeGet() == SPI_TRANSFER_MODE_16BIT)
        SPI2BUF = *((uint16_t*)pTransmitData);
    else
        SPI2BUF = *((uint8_t*)pTransmitData);

    while ( SPI2STATbits.SRXMPT == true);

    if (SPI2_TransferModeGet() == SPI_TRANSFER_MODE_16BIT)
        *((uint16_t*)pReceiveData) = SPI2BUF;
    else
        *((uint8_t*)pReceiveData) = SPI2BUF;

    return;
}


/**
 * @brief Exchange one element of data. Transmit or receive
 *
 * @param pTransmitData Data element to send. NULL when receiving
 * only.
 * @param pReceiveData  Data element to receive. NULL when
 * transmitting only.
 */
void SPI3_Exchange( uint8_t *pTransmitData, uint8_t *pReceiveData )
{

    while( SPI3STATbits.SPITBF == true )
    {

    }

    if (SPI3_TransferModeGet() == SPI_TRANSFER_MODE_16BIT)
        SPI3BUF = *((uint16_t*)pTransmitData);
    else
        SPI3BUF = *((uint8_t*)pTransmitData);

    while ( SPI3STATbits.SRXMPT == true);

    if (SPI3_TransferModeGet() == SPI_TRANSFER_MODE_16BIT)
        *((uint16_t*)pReceiveData) = SPI3BUF;
    else
        *((uint8_t*)pReceiveData) = SPI3BUF;

    return;
}


/**
 * @brief Exchange one element of data. Transmit or receive
 *
 * @param pTransmitData Data element to send. NULL when receiving
 * only.
 * @param pReceiveData  Data element to receive. NULL when
 * transmitting only.
 */
void SPI4_Exchange( uint8_t *pTransmitData, uint8_t *pReceiveData )
{

    while( SPI4STATbits.SPITBF == true )
    {

    }

    if (SPI4_TransferModeGet() == SPI_TRANSFER_MODE_16BIT)
        SPI4BUF = *((uint16_t*)pTransmitData);
    else
        SPI4BUF = *((uint8_t*)pTransmitData);

    while ( SPI4STATbits.SRXMPT == true);

    if (SPI4_TransferModeGet() == SPI_TRANSFER_MODE_16BIT)
        *((uint16_t*)pReceiveData) = SPI4BUF;
    else
        *((uint8_t*)pReceiveData) = SPI4BUF;

    return;
}


/**
 * @brief Exchange an entire buffer of data. Either transmit or
 * receive.
 *
 * @param pTransmitData Data array to send. NULL when receiving data
 * only.
 * @param byteCount     Number of bytes in the array.
 * @param pReceiveData  Array to receive the data. NULL when
 * transmitting data only.
 *
 * @return Number of data elements transmitted
 */
uint16_t SPI1_ExchangeBuffer(uint8_t *pTransmitData, uint16_t byteCount, uint8_t *pReceiveData)
{

    uint16_t dataSentCount = 0;
    uint16_t count = 0;
    uint16_t dummyDataReceived = 0;
    uint16_t dummyDataTransmit = SPI_DUMMY_DATA;

    uint8_t  *pSend, *pReceived;
    uint16_t addressIncrement;
    uint16_t receiveAddressIncrement, sendAddressIncrement;

    SPI_TRANSFER_MODE spiModeStatus;

    spiModeStatus = SPI1_TransferModeGet();
    // set up the address increment variable
    if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
    {
        addressIncrement = 2;
        byteCount >>= 1;
    }
    else
    {
        addressIncrement = 1;
    }

    // set the pointers and increment delta
    // for transmit and receive operations
    if (pTransmitData == NULL)
    {
        sendAddressIncrement = 0;
        pSend = (uint8_t*)&dummyDataTransmit;
    }
    else
    {
        sendAddressIncrement = addressIncrement;
        pSend = (uint8_t*)pTransmitData;
    }

    if (pReceiveData == NULL)
    {
       receiveAddressIncrement = 0;
       pReceived = (uint8_t*)&dummyDataReceived;
    }
    else
    {
       receiveAddressIncrement = addressIncrement;
       pReceived = (uint8_t*)pReceiveData;
    }


    while( SPI1STATbits.SPITBF == true )
    {

    }

    while (dataSentCount < byteCount)
    {
        if ((count < SPI_FIFO_FILL_LIMIT))
        {
            if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
                SPI1BUF = *((uint16_t*)pSend);
            else
                SPI1BUF = *pSend;
            pSend += sendAddressIncrement;
            dataSentCount++;
            count++;
        }

        if (SPI1STATbits.SRXMPT == false)
        {
            if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
                *((uint16_t*)pReceived) = SPI1BUF;
            else
                *pReceived = SPI1BUF;
            pReceived += receiveAddressIncrement;
            count--;
        }

    }
    while (count)
    {
        if (SPI1STATbits.SRXMPT == false)
        {
            if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
                *((uint16_t*)pReceived) = SPI1BUF;
            else
                *pReceived = SPI1BUF;
            pReceived += receiveAddressIncrement;
            count--;
        }
    }

    return dataSentCount;
}

/**
 * @brief Exchange an entire buffer of data. Either transmit or
 * receive.
 *
 * @param pTransmitData Data array to send. NULL when receiving data
 * only.
 * @param byteCount     Number of bytes in the array.
 * @param pReceiveData  Array to receive the data. NULL when
 * transmitting data only.
 *
 * @return Number of data elements transmitted
 */
uint16_t SPI2_ExchangeBuffer(uint8_t *pTransmitData, uint16_t byteCount, uint8_t *pReceiveData)
{

    uint16_t dataSentCount = 0;
    uint16_t count = 0;
    uint16_t dummyDataReceived = 0;
    uint16_t dummyDataTransmit = SPI_DUMMY_DATA;

    uint8_t  *pSend, *pReceived;
    uint16_t addressIncrement;
    uint16_t receiveAddressIncrement, sendAddressIncrement;

    SPI_TRANSFER_MODE spiModeStatus;

    spiModeStatus = SPI2_TransferModeGet();
    // set up the address increment variable
    if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
    {
        addressIncrement = 2;
        byteCount >>= 1;
    }
    else
    {
        addressIncrement = 1;
    }

    // set the pointers and increment delta
    // for transmit and receive operations
    if (pTransmitData == NULL)
    {
        sendAddressIncrement = 0;
        pSend = (uint8_t*)&dummyDataTransmit;
    }
    else
    {
        sendAddressIncrement = addressIncrement;
        pSend = (uint8_t*)pTransmitData;
    }

    if (pReceiveData == NULL)
    {
       receiveAddressIncrement = 0;
       pReceived = (uint8_t*)&dummyDataReceived;
    }
    else
    {
       receiveAddressIncrement = addressIncrement;
       pReceived = (uint8_t*)pReceiveData;
    }


    while( SPI2STATbits.SPITBF == true )
    {

    }

    while (dataSentCount < byteCount)
    {
        if ((count < SPI_FIFO_FILL_LIMIT))
        {
            if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
                SPI2BUF = *((uint16_t*)pSend);
            else
                SPI2BUF = *pSend;
            pSend += sendAddressIncrement;
            dataSentCount++;
            count++;
        }

        if (SPI2STATbits.SRXMPT == false)
        {
            if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
                *((uint16_t*)pReceived) = SPI2BUF;
            else
                *pReceived = SPI2BUF;
            pReceived += receiveAddressIncrement;
            count--;
        }

    }
    while (count)
    {
        if (SPI2STATbits.SRXMPT == false)
        {
            if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
                *((uint16_t*)pReceived) = SPI2BUF;
            else
                *pReceived = SPI2BUF;
            pReceived += receiveAddressIncrement;
            count--;
        }
    }

    return dataSentCount;
}
/**
 * @brief Exchange an entire buffer of data. Either transmit or
 * receive.
 *
 * @param pTransmitData Data array to send. NULL when receiving data
 * only.
 * @param byteCount     Number of bytes in the array.
 * @param pReceiveData  Array to receive the data. NULL when
 * transmitting data only.
 *
 * @return Number of data elements transmitted
 */
uint16_t SPI3_ExchangeBuffer(uint8_t *pTransmitData, uint16_t byteCount, uint8_t *pReceiveData)
{

    uint16_t dataSentCount = 0;
    uint16_t count = 0;
    uint16_t dummyDataReceived = 0;
    uint16_t dummyDataTransmit = SPI_DUMMY_DATA;

    uint8_t  *pSend, *pReceived;
    uint16_t addressIncrement;
    uint16_t receiveAddressIncrement, sendAddressIncrement;

    SPI_TRANSFER_MODE spiModeStatus;

    spiModeStatus = SPI3_TransferModeGet();
    // set up the address increment variable
    if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
    {
        addressIncrement = 2;
        byteCount >>= 1;
    }
    else
    {
        addressIncrement = 1;
    }

    // set the pointers and increment delta
    // for transmit and receive operations
    if (pTransmitData == NULL)
    {
        sendAddressIncrement = 0;
        pSend = (uint8_t*)&dummyDataTransmit;
    }
    else
    {
        sendAddressIncrement = addressIncrement;
        pSend = (uint8_t*)pTransmitData;
    }

    if (pReceiveData == NULL)
    {
       receiveAddressIncrement = 0;
       pReceived = (uint8_t*)&dummyDataReceived;
    }
    else
    {
       receiveAddressIncrement = addressIncrement;
       pReceived = (uint8_t*)pReceiveData;
    }


    while( SPI3STATbits.SPITBF == true )
    {

    }

    while (dataSentCount < byteCount)
    {
        if ((count < SPI_FIFO_FILL_LIMIT))
        {
            if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
                SPI3BUF = *((uint16_t*)pSend);
            else
                SPI3BUF = *pSend;
            pSend += sendAddressIncrement;
            dataSentCount++;
            count++;
        }

        if (SPI3STATbits.SRXMPT == false)
        {
            if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
                *((uint16_t*)pReceived) = SPI3BUF;
            else
                *pReceived = SPI3BUF;
            pReceived += receiveAddressIncrement;
            count--;
        }

    }
    while (count)
    {
        if (SPI3STATbits.SRXMPT == false)
        {
            if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
                *((uint16_t*)pReceived) = SPI3BUF;
            else
                *pReceived = SPI3BUF;
            pReceived += receiveAddressIncrement;
            count--;
        }
    }

    return dataSentCount;
}
/**
 * @brief Exchange an entire buffer of data. Either transmit or
 * receive.
 *
 * @param pTransmitData Data array to send. NULL when receiving data
 * only.
 * @param byteCount     Number of bytes in the array.
 * @param pReceiveData  Array to receive the data. NULL when
 * transmitting data only.
 *
 * @return Number of data elements transmitted
 */
uint16_t SPI4_ExchangeBuffer(uint8_t *pTransmitData, uint16_t byteCount, uint8_t *pReceiveData)
{

    uint16_t dataSentCount = 0;
    uint16_t count = 0;
    uint16_t dummyDataReceived = 0;
    uint16_t dummyDataTransmit = SPI_DUMMY_DATA;

    uint8_t  *pSend, *pReceived;
    uint16_t addressIncrement;
    uint16_t receiveAddressIncrement, sendAddressIncrement;

    SPI_TRANSFER_MODE spiModeStatus;

    spiModeStatus = SPI4_TransferModeGet();
    // set up the address increment variable
    if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
    {
        addressIncrement = 2;
        byteCount >>= 1;
    }
    else
    {
        addressIncrement = 1;
    }

    // set the pointers and increment delta
    // for transmit and receive operations
    if (pTransmitData == NULL)
    {
        sendAddressIncrement = 0;
        pSend = (uint8_t*)&dummyDataTransmit;
    }
    else
    {
        sendAddressIncrement = addressIncrement;
        pSend = (uint8_t*)pTransmitData;
    }

    if (pReceiveData == NULL)
    {
       receiveAddressIncrement = 0;
       pReceived = (uint8_t*)&dummyDataReceived;
    }
    else
    {
       receiveAddressIncrement = addressIncrement;
       pReceived = (uint8_t*)pReceiveData;
    }


    while( SPI4STATbits.SPITBF == true )
    {

    }

    while (dataSentCount < byteCount)
    {
        if ((count < SPI_FIFO_FILL_LIMIT))
        {
            if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
                SPI4BUF = *((uint16_t*)pSend);
            else
                SPI4BUF = *pSend;
            pSend += sendAddressIncrement;
            dataSentCount++;
            count++;
        }

        if (SPI4STATbits.SRXMPT == false)
        {
            if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
                *((uint16_t*)pReceived) = SPI4BUF;
            else
                *pReceived = SPI4BUF;
            pReceived += receiveAddressIncrement;
            count--;
        }

    }
    while (count)
    {
        if (SPI4STATbits.SRXMPT == false)
        {
            if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
                *((uint16_t*)pReceived) = SPI4BUF;
            else
                *pReceived = SPI4BUF;
            pReceived += receiveAddressIncrement;
            count--;
        }
    }

    return dataSentCount;
}
/**
 * @brief Get SPI1 transfer mode
 *
 */
inline __attribute__((__always_inline__)) SPI_TRANSFER_MODE SPI1_TransferModeGet(void)
{
	if (SPI1CON1bits.MODE16 == 0)
        return SPI_TRANSFER_MODE_8BIT;
    else
        return SPI_TRANSFER_MODE_16BIT;
}

/**
 * @brief Get SPI1 transfer mode
 *
 */
inline __attribute__((__always_inline__)) SPI_TRANSFER_MODE SPI2_TransferModeGet(void)
{
	if (SPI2CON1bits.MODE16 == 0)
        return SPI_TRANSFER_MODE_8BIT;
    else
        return SPI_TRANSFER_MODE_16BIT;
}

/**
 * @brief Get SPI1 transfer mode
 *
 */
inline __attribute__((__always_inline__)) SPI_TRANSFER_MODE SPI3_TransferModeGet(void)
{
	if (SPI3CON1bits.MODE16 == 0)
        return SPI_TRANSFER_MODE_8BIT;
    else
        return SPI_TRANSFER_MODE_16BIT;
}

/**
 * @brief Get SPI1 transfer mode
 *
 */
inline __attribute__((__always_inline__)) SPI_TRANSFER_MODE SPI4_TransferModeGet(void)
{
	if (SPI4CON1bits.MODE16 == 0)
        return SPI_TRANSFER_MODE_8BIT;
    else
        return SPI_TRANSFER_MODE_16BIT;
}
#endif
