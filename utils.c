#include <stdint.h>

uint16_t popcount(uint16_t x)
{
uint16_t c = 0;
for (; x > 0; x &= x -1) c++;
return c;
}

