#ifndef NOCAN

/*
 *   lib_pic33f
 *
 *   CAN
 *      - module configuration
 *      - send and receive functions
 *   ______________________________________________________________
 *
 * CAN bit timings
 *	In the CAN bus the propagation time of a bit is given by the sum of the
 *	duration of 4 phases:
 *
 *	|--------------|---------------------|-----------------|-----------------|
 *	| Sync Segment | Propagation Segment | Phase 1 Segment | Phase 2 Segment |
 *	|--------------|---------------------|-----------------|-----------------|
 *                                                        / \
 *	                                                       |
 *	                                                       |
 *	                                                  Sample point 
 * 
 *  |------------------------------------------------------------------------|
 *  |                                 1 bit                                  |
 *
 *  The basic unit in which we measure the length of each phase is called the
 *  time quanta (TQ). 
 *  The propagation of a bit as at least 8 TQ and at most 25 TQ.
 *  The sync segment is fixed at 1TQ.
 *  The propagation segment and the phase 1 segment can be configured between
 *  1-8TQ.
 *  The phase 2 segment can be configured between 2-8 TQ.
 *
 *  The sample point is the point in time in each we expect the bit to be
 *  sampled. The ECAN module allows us to choose between sampling once and
 *  thrice for increased accuracy.
 *	It is recommended to keep the sample point between 80% and 90% of the total
 *	bit length.
 *
 *	The syncronization jump width is how much we allow the bit time to be
 *	shortened or lengthened to keep the bus synchronous.
 *	It is recommend that it as big as long as it is smaller than phase 2.
 *
 *  
 *  http://ww1.microchip.com/downloads/en/AppNotes/00754.pdf
 *  https://vector.com/portal/medien/cmc/application_notes/AN-AND-1-106_Basic_CAN_Bit_Timing.pdf
 */

#include <lib_pic33e/can.h>
#include <lib_pic33e/timing.h>
#include <stdlib.h>
#include <stdbool.h>
#include <xc.h>

#include <lib_pic33e/utils.h>
#include <lib_pic33e/priority.h>
#include <lib_pic33e/pps.h>

#include <libpic30.h>

/* Allow the buffer sizes to be set by the makefile, default to a sane
 * behaviour */
#ifndef CAN1_RX_SIZE
#define CAN1_RX_SIZE 64
#endif

#ifndef CAN2_RX_SIZE
#define CAN2_RX_SIZE 64
#endif


/* CAN receive circular ring buffers 
 * Here used to increase tolerance to spikes in the number of messages in the
 * bus*/
static volatile CANdata can2_rx_ring[CAN2_RX_SIZE];
static volatile unsigned can2_rx_read=0, can2_rx_write=0;

static volatile CANdata can1_rx_ring[CAN1_RX_SIZE];
static volatile unsigned can1_rx_read=0, can1_rx_write=0;


/* DMA buffers declaration. Alloced in RAM and access point for message sending
 * and receiving between our code and the CAN module
 */
#define ECAN_BUFFER_SIZE 16
__eds__ unsigned can1_msg_buffer[ECAN_BUFFER_SIZE][8] __attribute__((
			__eds__, aligned(ECAN_BUFFER_SIZE * 16)));

__eds__ unsigned can2_msg_buffer[ECAN_BUFFER_SIZE][8] __attribute__((
			__eds__, aligned(ECAN_BUFFER_SIZE * 16)));

/* Default CAN configuration
 * Creates a config structure with a baudrate of 1 MHz and 8TQ distributed in this fashion:
 *		propagation segment : 2 TQ
 *		phase 1 segment     : 3 TQ
 *		phase 2 segment		: 2 TQ
 * The sample point is at 75%, triple sampling is not configured.
 *
 * args    : config   - CANconfig struct to be filled with the default values
 * returns : CANerror - Error value
 */
CANerror
default_can_config(CANconfig *config) {
	if (config == NULL)
		return ENULL;

	config->sync_jump_width = 1;
	config->baudrate = 1000000UL;
	config->prop_seg = 2;
	config->phase1_seg = 3;
	config->phase2_seg = 2;
	config->triple_sampling = 0;
	/* 16 buffers in fifo starting at position 16 */
	config->fifo_size = 0x6;
	config->fifo_start = 0x8;

	config->interrupt_priority = 6;
	config->rx_interrupt_priority = 6;

	config->number_masks = 1;
	config->filter_masks[0] = 0;

	config->number_filters = 1;
	config->filter_sids[0] = 0;
	config->filter_mask_source[0] = 0;
	return SUCCESS;
}


/* Verify baudrate parameters
 * Checks if bit timing segments respect the limits
 * Sets the CANCKS and BRP values
 * args    : config     - CANconfig configuration
 *         : cancks     - cancks value to be set
 *         : brp        - brp value to be set
 * returns : CANerror   - Error code
 */
CANerror 
set_baudrate(CANconfig *config, uint16_t *cancks, uint16_t *brp) {

	/* Check the bit timings for configuration errors */
	if (config->prop_seg > 8 || config->prop_seg < 1)
		return -1;
	if (config->phase1_seg > 8 || config->phase1_seg < 1)
		return -1;
	if (config->phase2_seg > 8 || config->phase2_seg < 2)
		return -1;

	/* We know that the number of TQ is sync+prop+phase1+phase2, where sync=1
	 * and the others are user configurable.
	 */
	uint8_t n_tq =1+config->prop_seg+config->phase1_seg+config->phase2_seg;
	if (n_tq > 25 || n_tq < 8)
		return ETIMING;

	/* The baudrate prescaler can be calculated from the desired baudrate, the
	 * configured number of time quantas (TQ) and F_CAN.  If the baudrate
	 * prescaller is smaller than 0 then some magic needs to happen: If 1/BRP-1
	 * = 1 then configuration is still possible, we only need to change the
	 * module frequency with CANCKS. If it is bigger than 1 then the
	 * configuration is impossible and we need to give up
	 */
	int16_t brp_limit = 2*n_tq*config->baudrate/FCY-1;
	if (brp_limit > 1)
		return ECONFIG;
	else if (brp_limit == 1)
		*cancks = 0;

	if (*cancks)
		*brp = FCY/(2*n_tq*config->baudrate)-1;
	else
		*brp = FCY/(n_tq*config->baudrate)-1;

	return 0;
}


/* Config DMA channel 0 for CAN1 trasmission */
void
config_dma0() {
	DMA0CONbits.SIZE = 0x0;
	DMA0CONbits.DIR = 0x1;
	DMA0CONbits.AMODE = 0x2;
	DMA0CONbits.MODE = 0x0;
	DMA0REQ = 0x46;
	DMA0CNT = 7;
	DMA0PAD = (volatile unsigned int) &C1TXD;
	DMA0STAL = (unsigned int) can1_msg_buffer;
    DMA0STAH = 0;

	IFS0bits.DMA0IF = 0;
	IEC0bits.DMA0IE = 0;

	/* Configure buffer 0 to 7 as send buffers */
	C1TR01CONbits.TXEN0 = 0x1;
	C1TR01CONbits.TXEN1 = 0x1;
	C1TR23CONbits.TXEN2 = 0x1;
	C1TR23CONbits.TXEN3 = 0x1;
	C1TR45CONbits.TXEN4 = 0x1;
	C1TR45CONbits.TXEN5 = 0x1;
	C1TR67CONbits.TXEN6 = 0x1;
	C1TR67CONbits.TXEN7 = 0x1;

	/* Assign buffer priorities */
	C1TR01CONbits.TX0PRI = 0x3;
	C1TR01CONbits.TX1PRI = 0x2;
	C1TR23CONbits.TX2PRI = 0x1;
	C1TR23CONbits.TX3PRI = 0x0;
	C1TR45CONbits.TX4PRI = 0x3;
	C1TR45CONbits.TX5PRI = 0x2;
	C1TR67CONbits.TX6PRI = 0x1;
	C1TR67CONbits.TX7PRI = 0x0;

	DMA0CONbits.CHEN = 0x1;
}

/* Config DMA channel 1 for CAN1 reception */
void 
config_dma1() {
	DMA1CONbits.SIZE = 0x0;
	DMA1CONbits.DIR = 0x0;
	DMA1CONbits.AMODE = 0x2;
	DMA1CONbits.MODE = 0x0;
	DMA1REQ = 0x22;
	DMA1CNT = 7;
	DMA1PAD = (volatile unsigned int) &C1RXD;
	DMA1STAL = (unsigned int) can1_msg_buffer;
	DMA1STAH = 0;
	DMA1CONbits.CHEN = 0x1;
}

/* DMA0 interrupt */
void __attribute__ ((interrupt, auto_psv, shadow)) 
_DMA0Interrupt( void )
{
	IFS0bits.DMA0IF = 0;    // Clear the DMA0 Interrupt Flag;
}

/* DMA1 interrupt */
void __attribute__ ((interrupt, auto_psv, shadow)) 
_DMA1Interrupt( void )
{
	IFS0bits.DMA1IF = 0;    // Clear the DMA1 Interrupt Flag;
}

/* Config DMA channel 2 for CAN2 trasmission */
void
config_dma2(void) {
	DMA2CONbits.SIZE = 0x0;
	DMA2CONbits.DIR = 0x1;
	DMA2CONbits.AMODE = 0x2;
    DMA2CONbits.MODE = 0x0;
	DMA2REQ = 0x47;
    DMA2CNT = 7;
    DMA2PAD = (volatile unsigned int) &C2TXD;
    DMA2STAL = __builtin_dmaoffset(can2_msg_buffer);
    DMA2STAH = __builtin_dmapage(can2_msg_buffer);

	IFS1bits.DMA2IF = 0;
	IEC1bits.DMA2IE = 0;

	/* Configure buffer 0 to 7 as send buffers */
	C2TR01CONbits.TXEN0 = 0x1;
	C2TR01CONbits.TXEN1 = 0x1;
	C2TR23CONbits.TXEN2 = 0x1;
	C2TR23CONbits.TXEN3 = 0x1;
	C2TR45CONbits.TXEN4 = 0x1;
	C2TR45CONbits.TXEN5 = 0x1;
	C2TR67CONbits.TXEN6 = 0x1;
	C2TR67CONbits.TXEN7 = 0x1;

	/* Assign buffer priorities */
	C2TR01CONbits.TX0PRI = 0x3;
	C2TR01CONbits.TX1PRI = 0x2;
	C2TR23CONbits.TX2PRI = 0x1;
	C2TR23CONbits.TX3PRI = 0x0;
	C2TR45CONbits.TX4PRI = 0x3;
	C2TR45CONbits.TX5PRI = 0x2;
	C2TR67CONbits.TX6PRI = 0x1;
	C2TR67CONbits.TX7PRI = 0x0;

	DMA2CONbits.CHEN = 0x1; // Activate DMA
}

/* Config DMA channel 3 for CAN2 reception */
void
config_dma3(void) {
	DMA3CONbits.SIZE = 0x0;
	DMA3CONbits.DIR = 0x0;
	DMA3CONbits.AMODE = 0x2;
    DMA3CONbits.MODE = 0x0;
	DMA3REQ = 0x37;
    DMA3CNT = 7;
    DMA3PAD = (volatile unsigned int) &C2RXD;
    DMA3STAL = __builtin_dmaoffset(can2_msg_buffer);
    DMA3STAH = __builtin_dmapage(can2_msg_buffer);
	DMA3STAH = 0;
	DMA3CONbits.CHEN = 0x1; // Activate DMA
}

/* DMA2 interrupt */
void __attribute__ ( (interrupt, auto_psv, shadow) ) 
_DMA2Interrupt( void )
{
	IFS1bits.DMA2IF = 0;    // Clear the DMA1 Interrupt Flag;
}

/* DMA3 interrupt */
void __attribute__ ( (interrupt, auto_psv, shadow) ) 
_DMA3Interrupt( void )
{
	IFS2bits.DMA3IF = 0;    // Clear the DMA1 Interrupt Flag;
}

void set_can_filters(uint16_t sid[16], uint16_t number_filters, CANconfig *config) {
	uint16_t i = 0;
	for (i = 0; i<number_filters; i++) {
		config->filter_sids[i] = sid[i];
		config->filter_mask_source[i] = 0;
	}

	config->filter_masks[0] = 0xFFFF;
	config->number_filters = number_filters;
	config->number_masks = 1;
}


void config_can1_filter_masks(CANconfig config) {
	switch (config.number_masks) {
		case 3:
			C1RXM2SIDbits.SID = config.filter_masks[2];
		case 2:
			C1RXM1SIDbits.SID = config.filter_masks[1];
		case 1:
			C1RXM0SIDbits.SID = config.filter_masks[0];
		case 0:
			break;
		default:
			break;
	}
}
void config_can1_filter_sids(CANconfig config) {
	switch (config.number_filters) {
		case 16:
			C1RXF15SIDbits.SID   = config.filter_sids[15];
		case 15:
			C1RXF14SIDbits.SID   = config.filter_sids[14];
		case 14:
			C1RXF13SIDbits.SID   = config.filter_sids[13];
		case 13:
			C1RXF12SIDbits.SID   = config.filter_sids[12];
		case 12:
			C1RXF11SIDbits.SID   = config.filter_sids[11];
		case 11:
			C1RXF10SIDbits.SID   = config.filter_sids[10];
		case 10:
			C1RXF9SIDbits.SID    = config.filter_sids[9];
		case 9:
			C1RXF8SIDbits.SID    = config.filter_sids[8];
		case 8:
			C1RXF7SIDbits.SID    = config.filter_sids[7];
		case 7:
			C1RXF6SIDbits.SID    = config.filter_sids[6];
		case 6:
			C1RXF5SIDbits.SID    = config.filter_sids[5];
		case 5:
			C1RXF4SIDbits.SID    = config.filter_sids[4];
		case 4:
			C1RXF3SIDbits.SID    = config.filter_sids[3];
		case 3:
			C1RXF2SIDbits.SID    = config.filter_sids[2];
		case 2:
			C1RXF1SIDbits.SID    = config.filter_sids[1];
		case 1:
			C1RXF0SIDbits.SID    = config.filter_sids[0];
		case 0:
			break;
		default:
			break;
	}
}

void config_can1_filter_mask_source(CANconfig config) {
	switch (config.number_filters) {
		case 16:
			C1FMSKSEL2bits.F15MSK = config.filter_mask_source[15];
		case 15:       
			C1FMSKSEL2bits.F14MSK = config.filter_mask_source[14];
		case 14:       
			C1FMSKSEL2bits.F13MSK = config.filter_mask_source[13];
		case 13:      
			C1FMSKSEL2bits.F12MSK = config.filter_mask_source[12];
		case 12:      
			C1FMSKSEL2bits.F11MSK = config.filter_mask_source[11];
		case 11:      
			C1FMSKSEL2bits.F10MSK = config.filter_mask_source[10];
		case 10:      
			C1FMSKSEL2bits.F9MSK  = config.filter_mask_source[9];
		case 9:     
			C1FMSKSEL2bits.F8MSK  = config.filter_mask_source[8];
		case 8:    
			C1FMSKSEL1bits.F7MSK  = config.filter_mask_source[7];
		case 7:   
			C1FMSKSEL1bits.F6MSK  = config.filter_mask_source[6];
		case 6:  
			C1FMSKSEL1bits.F5MSK  = config.filter_mask_source[5];
		case 5: 
			C1FMSKSEL1bits.F4MSK  = config.filter_mask_source[4];
		case 4:  
			C1FMSKSEL1bits.F3MSK  = config.filter_mask_source[3];
		case 3: 
			C1FMSKSEL1bits.F2MSK  = config.filter_mask_source[2];
		case 2:  
			C1FMSKSEL1bits.F1MSK  = config.filter_mask_source[1];
		case 1: 
			C1FMSKSEL1bits.F0MSK  = config.filter_mask_source[0];
		case 0: 
			break;
		default:
			break;
	}
}

void config_can1_filter_buffer_pointer(CANconfig config) {
	C1BUFPNT4bits.F15BP   = 0xF;
	C1BUFPNT4bits.F14BP   = 0xF;
	C1BUFPNT4bits.F13BP   = 0xF;
	C1BUFPNT4bits.F12BP   = 0xF;
	C1BUFPNT3bits.F11BP   = 0xF;
	C1BUFPNT3bits.F10BP   = 0xF;
	C1BUFPNT3bits.F9BP    = 0xF;
	C1BUFPNT3bits.F8BP    = 0xF;
	C1BUFPNT2bits.F7BP    = 0xF;
	C1BUFPNT2bits.F6BP    = 0xF;
	C1BUFPNT2bits.F5BP    = 0xF;
	C1BUFPNT2bits.F4BP    = 0xF;
	C1BUFPNT1bits.F3BP    = 0xF;
	C1BUFPNT1bits.F2BP    = 0xF;
	C1BUFPNT1bits.F1BP    = 0xF;
	C1BUFPNT1bits.F0BP    = 0xF;
}

void config_can1_filter_mide(CANconfig config) {
	C1RXM0SIDbits.MIDE   = 0x1;
	C1RXM1SIDbits.MIDE   = 0x1;
	C1RXM2SIDbits.MIDE   = 0x1;

	C1RXF0SIDbits.EXIDE = 0;
	C1RXF1SIDbits.EXIDE = 0;
	C1RXF2SIDbits.EXIDE = 0;
}

void config_can1_filter_filter_enable(CANconfig config) {
	switch (config.number_filters) {
		case 16:
			C1FEN1bits.FLTEN15   = 0x1;
		case 15:
			C1FEN1bits.FLTEN14   = 0x1;
		case 14:
			C1FEN1bits.FLTEN13   = 0x1;
		case 13:
			C1FEN1bits.FLTEN12   = 0x1;
		case 12:
			C1FEN1bits.FLTEN11   = 0x1;
		case 11:
			C1FEN1bits.FLTEN10   = 0x1;
		case 10:
			C1FEN1bits.FLTEN9    = 0x1;
		case 9:
			C1FEN1bits.FLTEN8    = 0x1;
		case 8:
			C1FEN1bits.FLTEN7    = 0x1;
		case 7:
			C1FEN1bits.FLTEN6    = 0x1;
		case 6:
			C1FEN1bits.FLTEN5    = 0x1;
		case 5:
			C1FEN1bits.FLTEN4    = 0x1;
		case 4:
			C1FEN1bits.FLTEN3    = 0x1;
		case 3:
			C1FEN1bits.FLTEN2    = 0x1;
		case 2:
			C1FEN1bits.FLTEN1    = 0x1;
		case 1:
			C1FEN1bits.FLTEN0    = 0x1;
		case 0:
			break;
		default:
			break;
	}
}

/* config can 1 filter */
void 
config_can1_filter(CANconfig config) {
	C1CTRL1bits.WIN      = 1;

	config_can1_filter_masks(config);
	config_can1_filter_sids(config);
	config_can1_filter_mask_source(config);
	config_can1_filter_buffer_pointer(config);
	config_can1_filter_mide(config);
	config_can1_filter_filter_enable(config);

	C1CTRL1bits.WIN      = 0x0;
	return;
}


uint16_t get_can1_mode() {
	return C1CTRL1bits.OPMODE;
}

void set_can1_mode(CANmodes mode) {
	C1CTRL1bits.REQOP = mode;
	while (get_can1_mode() != mode);
}

bool is_can1_enabled(void) {
	return !(C1CTRL1bits.OPMODE == DISABLE || C1CTRL1bits.OPMODE == CONFIGURATION);
}

/* Configure can 1 module.
 * __If no configuration is provided defaults to the configuration defined in
 * default_can_config
 * Checks if bit timings are whithin allowed values.

 * args    : config     - CANconfig configuration
 * returns : CANerror   - Error code
 */
CANerror
config_can1(CANconfig *config) {
	set_CPU_priority(7);
	
	CANconfig aux;

	if (config == NULL) {
		default_can_config(&aux);
		config = &aux;
	}

	uint16_t cancks = 1;
	uint16_t brp;

	uint16_t e = set_baudrate(config, &cancks, &brp);
	if (e) 
		return e;

	
	/* Request and wait for configuration mode */
	set_can1_mode(CONFIGURATION);

	/* Baudrate settings */
	C1CTRL1bits.CANCKS  = cancks;
	C1CFG1bits.BRP      = brp;
	
	/* bit timings */
	C1CFG1bits.SJW      = config->sync_jump_width-1;
	C1CFG2bits.PRSEG    = config->prop_seg-1;
	C1CFG2bits.SEG1PH   = config->phase1_seg-1;
	C1CFG2bits.SEG2PHTS = 1;
	C1CFG2bits.SEG2PH   = config->phase2_seg-1;
	C1CFG2bits.SAM      = config->triple_sampling;

	/* config TX/RX buffers */
	C1FCTRLbits.DMABS	= config->fifo_size;
	C1FCTRLbits.FSA     = config->fifo_start;

	IPC8bits.C1IP = config->interrupt_priority;
	IPC8bits.C1RXIP = config->rx_interrupt_priority;

	config_dma0();
	config_dma1();
	config_can1_filter(*config);

	/* Request and wait for normal mode */
	set_can1_mode(NORMAL);

	IEC2bits.C1IE = 1;
	C1INTEbits.RBIE = 1;
	C1INTEbits.TBIE = 1;
	C1INTEbits.WAKIE = 1;

	set_CPU_priority(0);
	
	return SUCCESS;
}


/* Finds if a CAN TX buffer is available to send a CAN message */
bool can1_available_buffer(int buffer) {
	switch (buffer) {
		case 0:
			return !C1TR01CONbits.TXREQ0;
		case 1:
			return !C1TR01CONbits.TXREQ1;
		case 2:
			return !C1TR23CONbits.TXREQ2;
		case 3:
			return !C1TR23CONbits.TXREQ3;
		case 4:
			return !C1TR45CONbits.TXREQ4;
		case 5:
			return !C1TR45CONbits.TXREQ5;
		case 6:
			return !C1TR67CONbits.TXREQ6;
		case 7:
			return !C1TR67CONbits.TXREQ7;
	}
	return 0;
}

/* Requests that a CAN message be sent on a TX buffer */
void 
request_can1_send(int buffer) {
	switch (buffer) {
		case 0:
			C1TR01CONbits.TXREQ0=1;
			break;
		case 1:
			C1TR01CONbits.TXREQ1=1;
			break;
		case 2:
			C1TR23CONbits.TXREQ2=1;
			break;
		case 3:
			C1TR23CONbits.TXREQ3=1;
			break;
		case 4:
			C1TR45CONbits.TXREQ4=1;
			break;
		case 5:
			C1TR45CONbits.TXREQ5=1;
			break;
		case 6:
			C1TR67CONbits.TXREQ6=1;
			break;
		case 7:
			C1TR67CONbits.TXREQ7=1;
			break;
	}
}

uint16_t can1_tx_available() {
	return 8 - C1TR01CONbits.TXREQ0 - C1TR01CONbits.TXREQ1 -
		C1TR23CONbits.TXREQ2 - C1TR23CONbits.TXREQ3 - C1TR45CONbits.TXREQ4 -
		C1TR45CONbits.TXREQ5 - C1TR67CONbits.TXREQ6 - C1TR67CONbits.TXREQ7;
}

/* Sends a CAN message.
 * Searches for available buffer, sends on the first found. If none is found
 * returns an ESENDFAILURE error
 *
 * args    : msg      - CANdata CAN message
 * returns : CANerror - Error code
 */
CANerror
send_can1(CANdata msg) {
	int send_buffer = -1;
	int i;
	for (i=0; i<8; i++) {
		if (can1_available_buffer(i)) {
			send_buffer = i;
			break;
		}
	}

	if (send_buffer == -1) {
		return ESENDFAILURE;
	}

	can1_msg_buffer[send_buffer][0] = msg.sid << 2;
	can1_msg_buffer[send_buffer][2] = msg.dlc & 0xF;
	can1_msg_buffer[send_buffer][3] = msg.data[0];
	can1_msg_buffer[send_buffer][4] = msg.data[1];
	can1_msg_buffer[send_buffer][5] = msg.data[2];
	can1_msg_buffer[send_buffer][6] = msg.data[3];

	request_can1_send(send_buffer);
	return SUCCESS;
}

/* Checks if there are any messages left to send in the CAN 1 circular ring
 * buffer 
 *
 * args: none
 * returns: bool - The function proposition
 */
bool 
can1_rx_empty() {
	return can1_rx_read == can1_rx_write;
}

/* Get a message from the circular ring buffer 
 * Always check if the buffer is empty with can1_rx_empty
 *
 * args    : none
 * returns : CANdata - CAN message
 */
CANdata
pop_can1() {
	unsigned aux = can1_rx_read;
	RING_NEXT(can1_rx_read, CAN1_RX_SIZE);
	return can1_rx_ring[aux];
}

/* CAN1 filter. Redefine this function in your code to create software filters
 * for CAN messages 
 *
 * args    : sid  - message standard id
 * returns : bool - proposition
 */
bool __attribute__((weak))
filter_can1(unsigned int sid) {
	return 1;
}


/* CAN 1 interrupt 
 * Handles the receiving of messages
 */
void __attribute__ ((interrupt, auto_psv, shadow)) _C1Interrupt( void )
{
	CANdata msg;
	int fifo_pointer = C1FIFObits.FNRB;
	unsigned int sid;

	if (C1INTFbits.TBIF) {
		C1INTFbits.TBIF = 0;
	}

	if (C1INTFbits.RBIF) {
		sid = can1_msg_buffer[fifo_pointer][0] >> 2;
		if (filter_can1(sid)) {
			msg.sid = can1_msg_buffer[fifo_pointer][0] >> 2;
			msg.dlc = can1_msg_buffer[fifo_pointer][2] & 0xF;
			msg.data[0] = can1_msg_buffer[fifo_pointer][3];
			msg.data[1] = can1_msg_buffer[fifo_pointer][4];
			msg.data[2] = can1_msg_buffer[fifo_pointer][5];
			msg.data[3] = can1_msg_buffer[fifo_pointer][6];

			can1_rx_ring[can1_rx_write] = msg;
			RING_NEXT(can1_rx_write, CAN1_RX_SIZE);

			if (fifo_pointer < 16) {
				C1RXFUL1 &= ~(1 << fifo_pointer);
			}
			else {
				C1RXFUL2 &= ~(1 << (fifo_pointer - 16));
			}
		}
		C1INTFbits.RBIF = 0;
	}

	if (C1INTFbits.WAKIF) {
		C1INTFbits.WAKIF = 0;
	}

	IFS2bits.C1IF = 0;      // clear interrupt flag
	C1INTF = 0;
}

void config_can2_filter_masks(CANconfig config) {
	switch (config.number_masks) {
		case 3:
			C2RXM2SIDbits.SID = config.filter_masks[2];
		case 2:
			C2RXM1SIDbits.SID = config.filter_masks[1];
		case 1:
			C2RXM0SIDbits.SID = config.filter_masks[0];
		case 0:
			break;
		default:
			break;
	}
}
void config_can2_filter_sids(CANconfig config) {
	switch (config.number_filters) {
		case 16:
			C2RXF15SIDbits.SID   = config.filter_sids[15];
		case 15:
			C2RXF14SIDbits.SID   = config.filter_sids[14];
		case 14:
			C2RXF13SIDbits.SID   = config.filter_sids[13];
		case 13:
			C2RXF12SIDbits.SID   = config.filter_sids[12];
		case 12:
			C2RXF11SIDbits.SID   = config.filter_sids[11];
		case 11:
			C2RXF10SIDbits.SID   = config.filter_sids[10];
		case 10:
			C2RXF9SIDbits.SID    = config.filter_sids[9];
		case 9:
			C2RXF8SIDbits.SID    = config.filter_sids[8];
		case 8:
			C2RXF7SIDbits.SID    = config.filter_sids[7];
		case 7:
			C2RXF6SIDbits.SID    = config.filter_sids[6];
		case 6:
			C2RXF5SIDbits.SID    = config.filter_sids[5];
		case 5:
			C2RXF4SIDbits.SID    = config.filter_sids[4];
		case 4:
			C2RXF3SIDbits.SID    = config.filter_sids[3];
		case 3:
			C2RXF2SIDbits.SID    = config.filter_sids[2];
		case 2:
			C2RXF1SIDbits.SID    = config.filter_sids[1];
		case 1:
			C2RXF0SIDbits.SID    = config.filter_sids[0];
		case 0:
			break;
		default:
			break;
	}
}

void config_can2_filter_mask_source(CANconfig config) {
	switch (config.number_filters) {
		case 16:
			C2FMSKSEL2bits.F15MSK = config.filter_mask_source[15];
		case 15:       
			C2FMSKSEL2bits.F14MSK = config.filter_mask_source[14];
		case 14:       
			C2FMSKSEL2bits.F13MSK = config.filter_mask_source[13];
		case 13:      
			C2FMSKSEL2bits.F12MSK = config.filter_mask_source[12];
		case 12:      
			C2FMSKSEL2bits.F11MSK = config.filter_mask_source[11];
		case 11:      
			C2FMSKSEL2bits.F10MSK = config.filter_mask_source[10];
		case 10:      
			C2FMSKSEL2bits.F9MSK  = config.filter_mask_source[9];
		case 9:     
			C2FMSKSEL2bits.F8MSK  = config.filter_mask_source[8];
		case 8:    
			C2FMSKSEL1bits.F7MSK  = config.filter_mask_source[7];
		case 7:   
			C2FMSKSEL1bits.F6MSK  = config.filter_mask_source[6];
		case 6:  
			C2FMSKSEL1bits.F5MSK  = config.filter_mask_source[5];
		case 5: 
			C2FMSKSEL1bits.F4MSK  = config.filter_mask_source[4];
		case 4:  
			C2FMSKSEL1bits.F3MSK  = config.filter_mask_source[3];
		case 3: 
			C2FMSKSEL1bits.F2MSK  = config.filter_mask_source[2];
		case 2:  
			C2FMSKSEL1bits.F1MSK  = config.filter_mask_source[1];
		case 1: 
			C2FMSKSEL1bits.F0MSK  = config.filter_mask_source[0];
		case 0: 
			break;
		default:
			break;
	}
}

void config_can2_filter_buffer_pointer(CANconfig config) {
	C2BUFPNT4bits.F15BP   = 0xF;
	C2BUFPNT4bits.F14BP   = 0xF;
	C2BUFPNT4bits.F13BP   = 0xF;
	C2BUFPNT4bits.F12BP   = 0xF;
	C2BUFPNT3bits.F11BP   = 0xF;
	C2BUFPNT3bits.F10BP   = 0xF;
	C2BUFPNT3bits.F9BP    = 0xF;
	C2BUFPNT3bits.F8BP    = 0xF;
	C2BUFPNT2bits.F7BP    = 0xF;
	C2BUFPNT2bits.F6BP    = 0xF;
	C2BUFPNT2bits.F5BP    = 0xF;
	C2BUFPNT2bits.F4BP    = 0xF;
	C2BUFPNT1bits.F3BP    = 0xF;
	C2BUFPNT1bits.F2BP    = 0xF;
	C2BUFPNT1bits.F1BP    = 0xF;
	C2BUFPNT1bits.F0BP    = 0xF;
}

void config_can2_filter_mide(CANconfig config) {
	C2RXM0SIDbits.MIDE   = 0x1;
	C2RXM1SIDbits.MIDE   = 0x1;
	C2RXM2SIDbits.MIDE   = 0x1;

	C2RXF0SIDbits.EXIDE = 0;
	C2RXF1SIDbits.EXIDE = 0;
	C2RXF2SIDbits.EXIDE = 0;
}

void config_can2_filter_filter_enable(CANconfig config) {
	switch (config.number_filters) {
		case 16:
			C2FEN1bits.FLTEN15   = 0x1;
		case 15:
			C2FEN1bits.FLTEN14   = 0x1;
		case 14:
			C2FEN1bits.FLTEN13   = 0x1;
		case 13:
			C2FEN1bits.FLTEN12   = 0x1;
		case 12:
			C2FEN1bits.FLTEN11   = 0x1;
		case 11:
			C2FEN1bits.FLTEN10   = 0x1;
		case 10:
			C2FEN1bits.FLTEN9    = 0x1;
		case 9:
			C2FEN1bits.FLTEN8    = 0x1;
		case 8:
			C2FEN1bits.FLTEN7    = 0x1;
		case 7:
			C2FEN1bits.FLTEN6    = 0x1;
		case 6:
			C2FEN1bits.FLTEN5    = 0x1;
		case 5:
			C2FEN1bits.FLTEN4    = 0x1;
		case 4:
			C2FEN1bits.FLTEN3    = 0x1;
		case 3:
			C2FEN1bits.FLTEN2    = 0x1;
		case 2:
			C2FEN1bits.FLTEN1    = 0x1;
		case 1:
			C2FEN1bits.FLTEN0    = 0x1;
		case 0:
			break;
		default:
			break;
	}
}

/* config can 1 filter */
void 
config_can2_filter(CANconfig config) {
	C2CTRL1bits.WIN      = 1;
	
	config_can2_filter_masks(config);
	config_can2_filter_sids(config);
	config_can2_filter_mask_source(config);
	config_can2_filter_buffer_pointer(config);
	config_can2_filter_mide(config);
	config_can2_filter_filter_enable(config);

	C2CTRL1bits.WIN      = 0x0;
	return;
}

uint16_t get_can2_mode() {
	return C2CTRL1bits.OPMODE;
}

void set_can2_mode(CANmodes mode) {
	C2CTRL1bits.REQOP = mode;
	while (get_can2_mode() != mode);
}

bool is_can2_enabled(void) {
	return !(C2CTRL1bits.OPMODE == DISABLE || C2CTRL1bits.OPMODE == CONFIGURATION);
}

/* Configure can 2 module.
 * If no configuration is provided defaults to the configuration defined in
 * default_can_config
 * Checks if bit timings are whithin allowed values.
 *
 * args    : config   - CANconfig config structure
 * returns : CANerror - Error code
 */
CANerror
config_can2(CANconfig *config) {
	set_CPU_priority(7);

	CANconfig aux;

	if (config == NULL) {
		default_can_config(&aux);
		config = &aux;
	}

	uint16_t cancks = 1;
	uint16_t brp;

	uint16_t e = set_baudrate(config, &cancks, &brp);
	if (e) 
		return e;

	
	/* Request and wait for configuration mode */
	set_can2_mode(CONFIGURATION);

	/* Baudrate settings */
	C2CTRL1bits.CANCKS  = cancks;
	C2CFG1bits.BRP      = brp;
	
	/* bit timings */
	C2CFG1bits.SJW      = config->sync_jump_width-1;
	C2CFG2bits.PRSEG    = config->prop_seg-1;
	C2CFG2bits.SEG1PH   = config->phase1_seg-1;
	C2CFG2bits.SEG2PHTS = 1;
	C2CFG2bits.SEG2PH   = config->phase2_seg-1;
	C2CFG2bits.SAM      = config->triple_sampling;

	/* config TX/RX buffers */
	C2FCTRLbits.DMABS	= config->fifo_size;
	C2FCTRLbits.FSA     = config->fifo_start;

	IPC14bits.C2IP = config->interrupt_priority;
	IPC13bits.C2RXIP = config->rx_interrupt_priority;

	config_dma2();
	config_dma3();
	config_can2_filter(*config);

	/* Request and wait for normal mode */
	set_can2_mode(NORMAL);

	IEC3bits.C2IE = 1;
	C2INTEbits.RBIE = 1;
	C2INTEbits.TBIE = 1;
	C2INTEbits.WAKIE = 1;

	set_CPU_priority(0);
	
	return SUCCESS;
}


/* Finds if a CAN TX buffer is available to send a CAN message */
bool 
can2_available_buffer(int buffer) {
	switch (buffer) {
		case 0:
			return !C2TR01CONbits.TXREQ0;
		case 1:
			return !C2TR01CONbits.TXREQ1;
		case 2:
			return !C2TR23CONbits.TXREQ2;
		case 3:
			return !C2TR23CONbits.TXREQ3;
		case 4:
			return !C2TR45CONbits.TXREQ4;
		case 5:
			return !C2TR45CONbits.TXREQ5;
		case 6:
			return !C2TR67CONbits.TXREQ6;
		case 7:
			return !C2TR67CONbits.TXREQ7;
	}
	return 0;
}

/* Requests that a CAN message be sent on a TX buffer */
void 
request_can2_send(int buffer) {
	switch (buffer) {
		case 0:
			C2TR01CONbits.TXREQ0=1;
			break;
		case 1:
			C2TR01CONbits.TXREQ1=1;
			break;
		case 2:
			C2TR23CONbits.TXREQ2=1;
			break;
		case 3:
			C2TR23CONbits.TXREQ3=1;
			break;
		case 4:
			C2TR45CONbits.TXREQ4=1;
			break;
		case 5:
			C2TR45CONbits.TXREQ5=1;
			break;
		case 6:
			C2TR67CONbits.TXREQ6=1;
			break;
		case 7:
			C2TR67CONbits.TXREQ7=1;
			break;
	}
}

uint16_t can2_tx_available() {
	return 8 - C2TR01CONbits.TXREQ0 - C2TR01CONbits.TXREQ1 -
		C2TR23CONbits.TXREQ2 - C2TR23CONbits.TXREQ3 - C2TR45CONbits.TXREQ4 -
		C2TR45CONbits.TXREQ5 - C2TR67CONbits.TXREQ6 - C2TR67CONbits.TXREQ7;
}

/* Sends a CAN message.
 * Searches for available buffer, sends on the first found. If none is found
 * returns an ESENDFAILURE error
 *
 * args    : msg      - CANdata CAN message
 * returns : CANerror - Error code
 */
CANerror
send_can2(CANdata msg) {
	int send_buffer = -1;
	int i;
	for (i=0; i<8; i++) {
		if (can2_available_buffer(i)) {
			send_buffer = i;
			break;
		}
	}

	if (send_buffer == -1) {
		return ESENDFAILURE;
	}

	can2_msg_buffer[send_buffer][0] = msg.sid << 2;
	can2_msg_buffer[send_buffer][2] = msg.dlc & 0xF;
	can2_msg_buffer[send_buffer][3] = msg.data[0];
	can2_msg_buffer[send_buffer][4] = msg.data[1];
	can2_msg_buffer[send_buffer][5] = msg.data[2];
	can2_msg_buffer[send_buffer][6] = msg.data[3];

	request_can2_send(send_buffer);
	return SUCCESS;
}


/* Checks if there are any messages left to send in the CAN 2 circular ring
 * buffer 
 *
 * args    : none
 * returns : bool - The function proposition
 */
bool 
can2_rx_empty() {
	if (can2_rx_read == can2_rx_write) {
		return 1;
	}
	else {
		return 0;
	}
}

/* Get a message from the circular ring buffer 
 * Always check if the buffer is empty with can1_rx_empty
 *
 * args    : none
 * returns : CANdata - CAN message
 */
CANdata
pop_can2() {
	unsigned aux = can2_rx_read;
	RING_NEXT(can2_rx_read, CAN2_RX_SIZE);
	return can2_rx_ring[aux];
}

/* CAN2 filter. Redefine this function in your code to create software filters
 * for CAN messages 
 *
 * args    : sid  - message standard id
 * returns : bool - proposition
 */
bool __attribute__((weak))
filter_can2(unsigned int sid) {
	return 1;
}

/* CAN 2 interrupt 
 * Handles the receiving of messages
 */
void __attribute__ ((interrupt, auto_psv, shadow)) _C2Interrupt( void )
{
	CANdata msg;
	int fifo_pointer = C2FIFObits.FNRB;
	unsigned int sid;

	if (C2INTFbits.TBIF) {
		C2INTFbits.TBIF = 0;
	}

	if (C2INTFbits.RBIF) {
		sid = can2_msg_buffer[fifo_pointer][0] >> 2;
		if (filter_can2(sid)) {
			msg.sid = can2_msg_buffer[fifo_pointer][0] >> 2;
			msg.dlc = can2_msg_buffer[fifo_pointer][2] & 0xF;
			msg.data[0] = can2_msg_buffer[fifo_pointer][3];
			msg.data[1] = can2_msg_buffer[fifo_pointer][4];
			msg.data[2] = can2_msg_buffer[fifo_pointer][5];
			msg.data[3] = can2_msg_buffer[fifo_pointer][6];

			can2_rx_ring[can2_rx_write] = msg;
			RING_NEXT(can2_rx_write, CAN2_RX_SIZE);

			if (fifo_pointer < 16) {
				C2RXFUL1 &= ~(1 << fifo_pointer);
			}
			else {
				C2RXFUL2 &= ~(1 << (fifo_pointer - 16));
			}
		}
		C2INTFbits.RBIF = 0;
	}

	if (C2INTFbits.WAKIF) {
		C2INTFbits.WAKIF = 0;
	}

	IFS3bits.C2IF = 0;      // clear interrupt flag
	C2INTF = 0;
}

#endif
