#ifndef __FLASH__
#define __FLASH__

/**
 *! \file flash.h
 *  \brief Provides functions to deal with builtin flash memory
 *
 *   Copyright 2018 João Freitas <joao.m.freitas@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <xc.h>
#include <stdlib.h>
#include <libpic30.h>

/**
 * Flash error codes
 */
typedef enum _FlashError {FSUCCESS, FNULL_PTR, FREAD_ERROR, FOUT_OF_BOUNDS} FlashError;

/*! \fn write_flash(uint16_t *buffer, unsigned offset, unsigned len)
 * \brief Write data to flash. Writes to flash len 16 bit words from buffer at the offset position.
 * \param[in] buffer Data to be written to flash
 * \param[in] offset Offset from the beginning of the flash row
 * \param[in] len Number of 16 bit words to write to flash
 * \return Error code
 */
FlashError write_flash(uint16_t *buffer, unsigned offset, unsigned len);
/*! \fn read_flash(uint16_t *buffer, unsigned offset, unsigned len)
 * \brief Read data from flash. Reads len 16 bit words from flash at the offset position
 * \param[out] buffer Location to store the flash data
 * \param[in] offset Offset from the beginning of the flash row
 * \param[in] len Number of 16 bit words to read from flash
 * \return Error code
 */
FlashError read_flash (uint16_t *buffer, unsigned offset, unsigned len);

#endif
