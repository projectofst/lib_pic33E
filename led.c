#ifndef NOLED

#include "led.h"

#include <xc.h>
#include <ctype.h>

void config_rgb (void){
    TRIS_RGB_R=0;
    TRIS_RGB_G=0;
    TRIS_RGB_B=0;
}

void rgb_clear (void){

    LAT_RGB_G = 0;
    LAT_RGB_R = 0;
    LAT_RGB_B = 0;
}


/* Turns RGB on with color 'color':
   'r'- red    , 'g'- green   , 'b'- blue ,
   'y'- yellow , 'm'- magenta , 'c'- cyan , 
   'w'- white  , other - off 
*/

void rgb_on (char color){
    
    color = tolower(color);

    switch (color) {

        case 'r':
            LAT_RGB_G = 0;
            LAT_RGB_R = 1;
            LAT_RGB_B = 0;
            break;
    
        case 'g':
            LAT_RGB_G = 1;
            LAT_RGB_R = 0;
            LAT_RGB_B = 0;
            break;

        case 'b':
            LAT_RGB_G = 0;
            LAT_RGB_R = 0;
            LAT_RGB_B = 1;
            break;

        case 'y':
            LAT_RGB_G = 1;
            LAT_RGB_R = 1;
            LAT_RGB_B = 0;
            break;

        case 'm':
            LAT_RGB_G = 0;
            LAT_RGB_R = 1;
            LAT_RGB_B = 1;
            break;

        case 'c':
            LAT_RGB_G = 1;
            LAT_RGB_R = 0;
            LAT_RGB_B = 1;
            break;

        case 'w':
            LAT_RGB_G = 1;
            LAT_RGB_R = 1;
            LAT_RGB_B = 1;
            break;

        default:
            LAT_RGB_G = 0;
            LAT_RGB_R = 0;
            LAT_RGB_B = 0;
            break;
        }
}

#endif
