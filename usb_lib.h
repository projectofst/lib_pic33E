#ifndef _USB_LIB_H
#define _USB_LIB_H

#include "lib_pic33e/usb/system.h"
#include "lib_pic33e/usb/app_device_cdc_basic.h"
#include "lib_pic33e/usb/usb.h"
#include "lib_pic33e/usb/usb_device.h"
#include "lib_pic33e/usb/usb_device_cdc.h"

#define BUFFER_SIZE_USB 64

enum _uprintf_ERROR {
    STRING_OVER_MAX = 0,
    BUFFER_FULL     = 1,
};


extern uint8_t gpacket_buffer_usb[BUFFER_SIZE_USB][BULK_SIZE_USB];
extern uint8_t gpacket_last_pos_usb[BUFFER_SIZE_USB];
extern unsigned volatile write_index_usb;
extern unsigned volatile read_index_usb;
extern unsigned volatile inner_write_index_usb;


//Blue Led lights up when usb is successfully configured
#define USB_BLUE_LED

void USBInit();
bool usb_full();
void USBTasks();
void receive_handler_usb(char *readString);
void append_string_usb(uint8_t *buffer, size_t len);
int uprintf(char *format, ...);
#endif
