#ifndef NOPINASSIGNMENT

#include <xc.h>
#include "pps.h"

/*
 * See section 10. IO/Ports, subsection 4 of the family reference.
 * And section 11.4 IO/Ports of the datasheet
 */

void __attribute__((user_init)) config_pin_assignment(void)
{
    /*
     * Set all ANSELx registers to '0' so the digital ports are enabled by
     * default. Analog modules should set the needed ANSEL bits to '1'.
     */
    ANSELB = 0;
    ANSELC = 0;
    ANSELD = 0;
    ANSELE = 0;
    ANSELG = 0;

    return;
}

#endif
