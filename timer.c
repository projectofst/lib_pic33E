#ifndef NOTIMER

/**********************************************************************
 *   lib_pic30f
 *
 *   Timer
 *      - module configuration
 *      - interruption assignment
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#include <xc.h>
#include <limits.h>

#include "timer.h"

/**********************************************************************
 * Name:    config_timer1 (type A)
 * Args:    time - interrupt step in miliseconds; priority 0-7
 * Return:  -
 * Desc:    Configures and enables timer 1 module.
 **********************************************************************/
void config_timer1(unsigned int time, unsigned int priority){

	T1CONbits.TCS   = 0;		/* use internal clock: Fcy               */
	T1CONbits.TGATE = 0;		/* Gated mode off                        */
	T1CONbits.TCKPS = 3;		/* prescale 1:256                        */
	T1CONbits.TSIDL = 0;		/* don't stop the timer in idle          */

	TMR1 = 0;					/* clears the timer register             */
	PR1 = (M_SEC/256)*time;		/* value at which the register overflows */
								/* and raises T1IF                       */

	/* interruptions */
	IPC0bits.T1IP = priority;	/* Timer 1 Interrupt Priority 0-7        */
	IFS0bits.T1IF = 0;			/* clear interrupt flag                  */
	IEC0bits.T1IE = 1;			/* Timer 1 Interrupt Enable              */


	T1CONbits.TON = 1;			/* starts the timer                      */
	return;
}

/**********************************************************************
 * Name:    config_timer1 (type A)
 * Args:    time - interrupt step in microseconds; priority 0-7
 * Return:  -
 * Desc:    Configures timer 1 module.
 **********************************************************************/
void config_timer1_us(unsigned int time, unsigned int priority){

	T1CONbits.TCS   = 0;		/* use internal clock: Fcy               */
	T1CONbits.TGATE = 0;		/* Gated mode off                        */
	T1CONbits.TCKPS = 1;		/* prescale 1:8                          */
	T1CONbits.TSIDL = 0;		/* don't stop the timer in idle          */

	TMR1 = 0;					/* clears the timer register             */
	PR1 = U_SEC*time/8; 		/* value at which the register overflows */
								/* and raises T1IF                       */

	/* interruptions */
	IPC0bits.T1IP = priority;	/* Timer 1 Interrupt Priority 0-7        */
	IFS0bits.T1IF = 0;			/* clear interrupt flag                  */
	IEC0bits.T1IE = 1;			/* Timer 1 Interrupt Enable              */

	return;
}


/**********************************************************************
 * Name:    config_timer2 (type B)
 * Args:    time - interrupt step in miliseconds; priority 0-7
 * Return:  -
 * Desc:    Configures and enables timer 1 module.
 **********************************************************************/
void config_timer2(unsigned int time, unsigned int priority){

	T2CONbits.TCS   = 0;		/* use internal clock: Fcy               */
	T2CONbits.TGATE = 0;		/* Gated mode off                        */
	T2CONbits.TCKPS = 3;		/* prescale 1:256                        */
	T2CONbits.TSIDL = 0;		/* don't stop the timer in idle          */
	T2CONbits.T32 = 0;          /*TMRx and TMRy form 16-bit timer        */

	TMR2 = 0;					/* clears the timer register             */
	PR2 = (M_SEC/256)*time;		/* value at which the register overflows *
								 * and raises T1IF                       */

	/* interruptions */
	IPC1bits.T2IP = priority;	/* Timer 2 Interrupt Priority 0-7        */
	IFS0bits.T2IF = 0;			/* clear interrupt flag                  */
	IEC0bits.T2IE = 1;			/* Timer 2 Interrupt Enable              */


	T2CONbits.TON = 1;			/* starts the timer                      */
	return;
}

/**********************************************************************
 * Name:    config_timer2 (type B)
 * Args:    time - interrupt step in microseconds; priority 0-7
 * Return:  -
 * Desc:    Configures and enables timer 1 module.
 **********************************************************************/
void config_timer2_us(unsigned int time, unsigned int priority){

	T2CONbits.TCS   = 0;		/* use internal clock: Fcy               */
	T2CONbits.TGATE = 0;		/* Gated mode off                        */
	T2CONbits.TCKPS = 1;		/* prescale 1:8                          */
	T2CONbits.TSIDL = 0;		/* don't stop the timer in idle          */
	T2CONbits.T32 = 0;          /*TMRx and TMRy form 16-bit timer        */

	TMR2 = 0;					/* clears the timer register             */
	PR2 = U_SEC*time/8; 		/* value at which the register overflows *
								 * and raises T1IF                       */

	/* interruptions */
	IPC1bits.T2IP = priority;	/* Timer 2 Interrupt Priority 0-7        */
	IFS0bits.T2IF = 0;			/* clear interrupt flag                  */
	IEC0bits.T2IE = 1;			/* Timer 2 Interrupt Enable              */

	return;
}


/**********************************************************************
 * Name:    config_timer3		(type C)
 * Args:    time - interrupt step in miliseconds; priority 0-7
 * Return:  -
 * Desc:    Configures and enables timer 1 module.
 **********************************************************************/
void config_timer3(unsigned int time, unsigned int priority){

	T3CONbits.TCS   = 0;		/* use internal clock: Fcy               */
	T3CONbits.TGATE = 0;		/* Gated mode off                        */
	T3CONbits.TCKPS = 3;		/* prescale 1:256                        */
	T3CONbits.TSIDL = 0;		/* don't stop the timer in idle          */

	TMR3 = 0;					/* clears the timer register             */
	PR3 = (M_SEC/256)*time;		/* value at which the register overflows *
								 * and raises T1IF                       */

	/* interruptions */
	IPC2bits.T3IP = priority;	/* Timer 3 Interrupt Priority 0-7        */
	IFS0bits.T3IF = 0;			/* clear interrupt flag                  */
	IEC0bits.T3IE = 1;			/* Timer 3 Interrupt Enable              */


	T3CONbits.TON = 1;			/* starts the timer                      */
	return;
}

/**********************************************************************
 * Name:    config_timer3		(type C)
 * Args:    time - interrupt step in microseconds; priority 0-7
 * Return:  -
 * Desc:    Configures timer 1 module.
 **********************************************************************/
void config_timer3_us(unsigned int time, unsigned int priority){

	T3CONbits.TCS   = 0;		/* use internal clock: Fcy               */
	T3CONbits.TGATE = 0;		/* Gated mode off                        */
	T3CONbits.TCKPS = 1;		/* prescale 1:8                          */
	T3CONbits.TSIDL = 0;		/* don't stop the timer in idle          */

	TMR3 = 0;					/* clears the timer register             */
	PR3 = U_SEC*time/8; 		/* value at which the register overflows *
								 * and raises T1IF                       */

	/* interruptions */
	IPC2bits.T3IP = priority;	/* Timer 3 Interrupt Priority 0-7        */
	IFS0bits.T3IF = 0;			/* clear interrupt flag                  */
	IEC0bits.T3IE = 1;			/* Timer 3 Interrupt Enable              */

	return;
}

/**********************************************************************
 * Name:    config_timer4   (type B)
 * Args:    time - interrupt step in miliseconds; priority 0-7
 * Return:  -
 * Desc:    Configures and enables timer 1 module.
 **********************************************************************/
void config_timer4(unsigned int time, unsigned int priority){

	T4CONbits.TCS   = 0;		/* use internal clock: Fcy               */
	T4CONbits.TGATE = 0;		/* Gated mode off                        */
	T4CONbits.TCKPS = 3;		/* prescale 1:256                        */
	T4CONbits.TSIDL = 0;		/* don't stop the timer in idle          */
	T2CONbits.T32 = 0;          /*TMRx and TMRy form 16-bit timer        */

	TMR4 = 0;					/* clears the timer register             */
	PR4 = (M_SEC/256)*time;	    /* value at which the register overflows *
								 * and raises T1IF                       */

	/* interruptions */
	IPC6bits.T4IP = priority;	/* Timer 4 Interrupt Priority 0-7        */
	IFS1bits.T4IF = 0;			/* clear interrupt flag                  */
	IEC1bits.T4IE = 1;			/* Timer 4 Interrupt Enable              */


	T4CONbits.TON = 1;			/* starts the timer                      */
	return;
}

/**********************************************************************
 * Name:    config_timer4   (type B)
 * Args:    time - interrupt step in microseconds; priority 0-7
 * Return:  -
 * Desc:    Configures and enables timer 1 module.
 **********************************************************************/
void config_timer4_us(unsigned int time, unsigned int priority){

	T4CONbits.TCS   = 0;		/* use internal clock: Fcy               */
	T4CONbits.TGATE = 0;		/* Gated mode off                        */
	T4CONbits.TCKPS = 1;		/* prescale 1:8                          */
	T4CONbits.TSIDL = 0;		/* don't stop the timer in idle          */
	T2CONbits.T32 = 0;          /*TMRx and TMRy form 16-bit timer        */

	TMR4 = 0;					/* clears the timer register             */
	PR4 = U_SEC*time/8; 	    /* value at which the register overflows *
								 * and raises T1IF                       */

	/* interruptions */
	IPC6bits.T4IP = priority;	/* Timer 4 Interrupt Priority 0-7        */
	IFS1bits.T4IF = 0;			/* clear interrupt flag                  */
	IEC1bits.T4IE = 1;			/* Timer 4 Interrupt Enable              */

	return;
}

/**********************************************************************
 * Name:    config_timer5
 * Args:    time - interrupt step in miliseconds; priority 0-7
 * Return:  -
 * Desc:    Configures and enables timer 1 module.
 **********************************************************************/
void config_timer5(unsigned int time, unsigned int priority){

	T5CONbits.TCS   = 0;		/* use internal clock: Fcy               */
	T5CONbits.TGATE = 0;		/* Gated mode off                        */
	T5CONbits.TCKPS = 3;		/* prescale 1:256                        */
	T5CONbits.TSIDL = 0;		/* don't stop the timer in idle          */

	TMR5 = 0;					/* clears the timer register             */
	PR5 = (M_SEC/256)*time;		/* value at which the register overflows *
							     * and raises T1IF                       */

	/* interruptions */
	IPC7bits.T5IP = priority;	/* Timer 5 Interrupt Priority 0-7        */
	IFS1bits.T5IF = 0;			/* clear interrupt flag                  */
	IEC1bits.T5IE = 1;			/* Timer 5 Interrupt Enable              */


	T5CONbits.TON = 1;			/* starts the timer                      */
	return;
}

/**********************************************************************
 * Name:    config_timer5
 * Args:    time - interrupt step in microseconds; priority 0-7
 * Return:  -
 * Desc:    Configures timer 1 module.
 **********************************************************************/
void config_timer5_us(unsigned int time, unsigned int priority){

	T5CONbits.TCS   = 0;		/* use internal clock: Fcy               */
	T5CONbits.TGATE = 0;		/* Gated mode off                        */
	T5CONbits.TCKPS = 1;		/* prescale 1:8                          */
	T5CONbits.TSIDL = 0;		/* don't stop the timer in idle          */

	TMR5 = 0;					/* clears the timer register             */
	PR5 = U_SEC*time/8; 		/* value at which the register overflows *
							     * and raises T1IF                       */

	/* interruptions */
	IPC7bits.T5IP = priority;	/* Timer 5 Interrupt Priority 0-7        */
	IFS1bits.T5IF = 0;			/* clear interrupt flag                  */
	IEC1bits.T5IE = 1;			/* Timer 5 Interrupt Enable              */

	return;
}

/**********************************************************************
 * Name:    config_timer6 (type B)
 * Args:    time - interrupt step in miliseconds; priority 0-7
 * Return:  -
 * Desc:    Configures and enables timer 1 module.
 **********************************************************************/
void config_timer6(unsigned int time, unsigned int priority){

	T6CONbits.TCS   = 0;		/* use internal clock: Fcy               */
	T6CONbits.TGATE = 0;		/* Gated mode off                        */
	T6CONbits.TCKPS = 3;		/* prescale 1:256                        */
	T6CONbits.TSIDL = 0;		/* don't stop the timer in idle          */
	T6CONbits.T32 = 0;          /*TMRx and TMRy form 16-bit timer        */

	TMR6 = 0;					/* clears the timer register             */
	PR6 = (M_SEC/256)*time;		/* value at which the register overflows *
								 * and raises T1IF                       */

	/* interruptions */
	IPC11bits.T6IP = priority;	/* Timer 2 Interrupt Priority 0-7        */
	IFS2bits.T6IF = 0;			/* clear interrupt flag                  */
	IEC2bits.T6IE = 1;			/* Timer 2 Interrupt Enable              */


	T6CONbits.TON = 1;			/* starts the timer                      */
	return;
}

/**********************************************************************
 * Name:    config_timer6 (type B)
 * Args:    time - interrupt step in microseconds; priority 0-7
 * Return:  -
 * Desc:    Configures and enables timer 1 module.
 **********************************************************************/
void config_timer6_us(unsigned int time, unsigned int priority){

	T6CONbits.TCS   = 0;		/* use internal clock: Fcy               */
	T6CONbits.TGATE = 0;		/* Gated mode off                        */
	T6CONbits.TCKPS = 1;		/* prescale 1:8                          */
	T6CONbits.TSIDL = 0;		/* don't stop the timer in idle          */
	T6CONbits.T32 = 0;          /*TMRx and TMRy form 16-bit timer        */

	TMR6 = 0;					/* clears the timer register             */
	PR6 = U_SEC*time/8; 		/* value at which the register overflows *
								 * and raises T1IF                       */

	/* interruptions */
	IPC11bits.T6IP = priority;	/* Timer 2 Interrupt Priority 0-7        */
	IFS2bits.T6IF = 0;			/* clear interrupt flag                  */
	IEC2bits.T6IE = 1;			/* Timer 2 Interrupt Enable              */

	return;
}

/**********************************************************************
 * Name:    config_timer7		(type C)
 * Args:    time - interrupt step in miliseconds; priority 0-7
 * Return:  -
 * Desc:    Configures and enables timer 1 module.
 **********************************************************************/
void config_timer7(unsigned int time, unsigned int priority){

	T7CONbits.TCS   = 0;		/* use internal clock: Fcy               */
	T7CONbits.TGATE = 0;		/* Gated mode off                        */
	T7CONbits.TCKPS = 3;		/* prescale 1:256                        */
	T7CONbits.TSIDL = 0;		/* don't stop the timer in idle          */

	TMR7 = 0;					/* clears the timer register             */
	PR7 = (M_SEC/256)*time;		/* value at which the register overflows *
								 * and raises T1IF                       */

	/* interruptions */
	IPC12bits.T7IP = priority;	/* Timer 3 Interrupt Priority 0-7        */
	IFS3bits.T7IF = 0;			/* clear interrupt flag                  */
	IEC3bits.T7IE = 1;			/* Timer 3 Interrupt Enable              */


	T7CONbits.TON = 1;			/* starts the timer                      */
	return;
}

/**********************************************************************
 * Name:    config_timer7		(type C)
 * Args:    time - interrupt step in microseconds; priority 0-7
 * Return:  -
 * Desc:    Configures timer 1 module.
 **********************************************************************/
void config_timer7_us(unsigned int time, unsigned int priority){

	T7CONbits.TCS   = 0;		/* use internal clock: Fcy               */
	T7CONbits.TGATE = 0;		/* Gated mode off                        */
	T7CONbits.TCKPS = 1;		/* prescale 1:8                          */
	T7CONbits.TSIDL = 0;		/* don't stop the timer in idle          */

	TMR7 = 0;					/* clears the timer register             */
	PR7 = U_SEC*time/8; 		/* value at which the register overflows *
								 * and raises T1IF                       */

	/* interruptions */
	IPC12bits.T7IP = priority;	/* Timer 3 Interrupt Priority 0-7        */
	IFS3bits.T7IF = 0;			/* clear interrupt flag                  */
	IEC3bits.T7IE = 1;			/* Timer 3 Interrupt Enable              */

	return;
}

/**********************************************************************
 * Name:    config_timer8 (type B)
 * Args:    time - interrupt step in miliseconds; priority 0-7
 * Return:  -
 * Desc:    Configures and enables timer 1 module.
 **********************************************************************/
void config_timer8(unsigned int time, unsigned int priority){

	T8CONbits.TCS   = 0;		/* use internal clock: Fcy               */
	T8CONbits.TGATE = 0;		/* Gated mode off                        */
	T8CONbits.TCKPS = 3;		/* prescale 1:256                        */
	T8CONbits.TSIDL = 0;		/* don't stop the timer in idle          */
	T8CONbits.T32 = 0;          /*TMRx and TMRy form 16-bit timer        */

	TMR8 = 0;					/* clears the timer register             */
	PR8 = (M_SEC/256)*time;		/* value at which the register overflows *
								 * and raises T1IF                       */

	/* interruptions */
	IPC12bits.T8IP = priority;	/* Timer 2 Interrupt Priority 0-7        */
	IFS3bits.T8IF = 0;			/* clear interrupt flag                  */
	IEC3bits.T8IE = 1;			/* Timer 2 Interrupt Enable              */


	T8CONbits.TON = 1;			/* starts the timer                      */
	return;
}

/**********************************************************************
 * Name:    config_timer8 (type B)
 * Args:    time - interrupt step in microseconds; priority 0-7
 * Return:  -
 * Desc:    Configures and enables timer 1 module.
 **********************************************************************/
void config_timer8_us(unsigned int time, unsigned int priority){

	T8CONbits.TCS   = 0;		/* use internal clock: Fcy               */
	T8CONbits.TGATE = 0;		/* Gated mode off                        */
	T8CONbits.TCKPS = 1;		/* prescale 1:8                          */
	T8CONbits.TSIDL = 0;		/* don't stop the timer in idle          */
	T8CONbits.T32 = 0;          /*TMRx and TMRy form 16-bit timer        */

	TMR8 = 0;					/* clears the timer register             */
	PR8 = U_SEC*time/8; 		/* value at which the register overflows *
								 * and raises T1IF                       */

	/* interruptions */
	IPC12bits.T8IP = priority;	/* Timer 2 Interrupt Priority 0-7        */
	IFS3bits.T8IF = 0;			/* clear interrupt flag                  */
	IEC3bits.T8IE = 1;			/* Timer 2 Interrupt Enable              */

	return;
}

/**********************************************************************
 * Name:    config_timer3		(type C)
 * Args:    time - interrupt step in miliseconds; priority 0-7
 * Return:  -
 * Desc:    Configures and enables timer 1 module.
 **********************************************************************/
void config_timer9(unsigned int time, unsigned int priority){

	T9CONbits.TCS   = 0;		/* use internal clock: Fcy               */
	T9CONbits.TGATE = 0;		/* Gated mode off                        */
	T9CONbits.TCKPS = 3;		/* prescale 1:256                        */
	T9CONbits.TSIDL = 0;		/* don't stop the timer in idle          */

	TMR9 = 0;					/* clears the timer register             */
	PR9 = (M_SEC/256)*time;		/* value at which the register overflows *
								 * and raises T1IF                       */

	/* interruptions */
	IPC13bits.T9IP = priority;	/* Timer 3 Interrupt Priority 0-7        */
	IFS3bits.T9IF = 0;			/* clear interrupt flag                  */
	IEC3bits.T9IE = 1;			/* Timer 3 Interrupt Enable              */


	T9CONbits.TON = 1;			/* starts the timer                      */
	return;
}

/**********************************************************************
 * Name:    config_timer3		(type C)
 * Args:    time - interrupt step in microseconds; priority 0-7
 * Return:  -
 * Desc:    Configures timer 1 module.
 **********************************************************************/
void config_timer9_us(unsigned int time, unsigned int priority){

	T9CONbits.TCS   = 0;		/* use internal clock: Fcy               */
	T9CONbits.TGATE = 0;		/* Gated mode off                        */
	T9CONbits.TCKPS = 1;		/* prescale 1:8                          */
	T9CONbits.TSIDL = 0;		/* don't stop the timer in idle          */

	TMR9 = 0;					/* clears the timer register             */
	PR9 = U_SEC*time/8; 		/* value at which the register overflows *
								 * and raises T1IF                       */

	/* interruptions */
	IPC13bits.T9IP = priority;	/* Timer 3 Interrupt Priority 0-7        */
	IFS3bits.T9IF = 0;			/* clear interrupt flag                  */
	IEC3bits.T9IE = 1;			/* Timer 3 Interrupt Enable              */

	return;
}

/**
 * @brief Enables timer 1
 */
void enable_timer1(void){
    T1CONbits.TON = 1;
}
/**
 * @brief Enables timer 2
 */
void enable_timer2(void){
    T2CONbits.TON = 1;
}
/**
 * @brief Enables timer 3
 */
void enable_timer3(void){
    T3CONbits.TON = 1;
}
/**
 * @brief Enables timer 4
 */
void enable_timer4(void){
    T4CONbits.TON = 1;
}
/**
 * @brief Enables timer 5
 */
void enable_timer5(void){
    T5CONbits.TON = 1;
}
/**
 * @brief Enables timer 6
 */
void enable_timer6(void){
    T6CONbits.TON = 1;
}
/**
 * @brief Enables timer 7
 */
void enable_timer7(void){
    T7CONbits.TON = 1;
}
/**
 * @brief Enables timer 8
 */
void enable_timer8(void){
    T8CONbits.TON = 1;
}
/**
 * @brief Enables timer 9
 */
void enable_timer9(void){
    T9CONbits.TON = 1;
}

/**
 * @brief Disables timer 1
 */
void disable_timer1(void){
    T1CONbits.TON = 0;
}
/**
 * @brief Disables timer 2
 */
void disable_timer2(void){
    T2CONbits.TON = 0;
}
/**
 * @brief Disables timer 3
 */
void disable_timer3(void){
    T3CONbits.TON = 0;
}
/**
 * @brief Disables timer 4
 */
void disable_timer4(void){
    T4CONbits.TON = 0;
}
/**
 * @brief Disables timer 5
 */
void disable_timer5(void){
    T5CONbits.TON = 0;
}
/**
 * @brief Disables timer 6
 */
void disable_timer6(void){
    T6CONbits.TON = 0;
}
/**
 * @brief Disables timer 7
 */
void disable_timer7(void){
    T7CONbits.TON = 0;
}
/**
 * @brief Disables timer 8
 */
void disable_timer8(void){
    T8CONbits.TON = 0;
}
/**
 * @brief Disables timer 9
 */
void disable_timer9(void){
    T9CONbits.TON = 0;
}

/**
 * @brief Resets timer 1
 */
void reset_timer1(void){
    TMR1 = 0;
}
/**
 * @brief Resets timer 2
 */
void reset_timer2(void){
    TMR2 = 0;
}
/**
 * @brief Resets timer 3
 */
void reset_timer3(void){
    TMR3 = 0;
}
/**
 * @brief Resets timer 4
 */
void reset_timer4(void){
    TMR4 = 0;
}
/**
 * @brief Resets timer 5
 */
void reset_timer5(void){
    TMR5 = 0;
}
/**
 * @brief Resets timer 6
 */
void reset_timer6(void){
    TMR6 = 0;
}
/**
 * @brief Resets timer 7
 */
void reset_timer7(void){
    TMR7 = 0;
}
/**
 * @brief Resets timer 8
 */
void reset_timer8(void){
    TMR8 = 0;
}
/**
 * @brief Resets timer 9
 */
void reset_timer9(void){
    TMR9 = 0;
}

void __attribute__((weak)) timer1_callback(void) {
	return;
}

void  __attribute__((weak)) timer2_callback(void){
	return;
}

void __attribute__((weak)) timer3_callback(void){
	return;
}

void __attribute__((weak)) timer4_callback(void){
	return;
}

void __attribute__((weak)) timer5_callback(void){
	return;
}

void __attribute__((weak)) timer6_callback(void){
	return;
}

void __attribute__((weak)) timer7_callback(void){
	return;
}

void __attribute__((weak)) timer8_callback(void){
	return;
}

void __attribute__((weak)) timer9_callback(void){
	return;
}

/**********************************************************************
 * Assign Timer 1 interruption
 **********************************************************************/
void __attribute__((interrupt, auto_psv, shadow)) _T1Interrupt (void){
	timer1_callback();
	IFS0bits.T1IF = 0;			/* clear interrupt flag                  */
	return;
}

/**********************************************************************
 * Assign Timer 2 interruption
 **********************************************************************/
void __attribute__((interrupt, auto_psv, shadow)) _T2Interrupt (void){
	timer2_callback();
	IFS0bits.T2IF = 0;			/* clear interrupt flag                  */

	return;
}

/**********************************************************************
 * Assign Timer 3 interruption
 **********************************************************************/
void __attribute__((interrupt, auto_psv, shadow)) _T3Interrupt (void){
	timer3_callback();
	IFS0bits.T3IF = 0;			/* clear interrupt flag                  */

	return;
}
/**********************************************************************
 * Assign Timer 4 interruption
 **********************************************************************/
void __attribute__((interrupt, auto_psv, shadow)) _T4Interrupt (void){
	timer4_callback();
	IFS1bits.T4IF = 0;			/* clear interrupt flag                  */

	return;
}
/**********************************************************************
 * Assign Timer 5 interruption
 **********************************************************************/
void __attribute__((interrupt, auto_psv, shadow)) _T5Interrupt (void){
	timer5_callback();
	IFS1bits.T5IF = 0;			/* clear interrupt flag                  */

	return;
}
/**********************************************************************
 * Assign Timer 6 interruption
 **********************************************************************/
void __attribute__((interrupt, auto_psv, shadow)) _T6Interrupt (void){
	timer6_callback();
	IFS2bits.T6IF = 0;			/* clear interrupt flag                  */

	return;
}
/**********************************************************************
 * Assign Timer 7 interruption
 **********************************************************************/
void __attribute__((interrupt, auto_psv, shadow)) _T7Interrupt (void){
	timer7_callback();
	IFS3bits.T7IF = 0;			/* clear interrupt flag                  */

	return;
}
/**********************************************************************
 * Assign Timer 8 interruption
 **********************************************************************/
void __attribute__((interrupt, auto_psv, shadow)) _T8Interrupt (void){
	timer8_callback();
	IFS3bits.T8IF = 0;			/* clear interrupt flag                  */

	return;
}
/**********************************************************************
 * Assign Timer 9 interruption
 **********************************************************************/
void __attribute__((interrupt, auto_psv, shadow)) _T9Interrupt (void){
	timer9_callback();
	IFS3bits.T9IF = 0;			/* clear interrupt flag                  */

	return;
}

#endif
