#ifndef _UTILS_H
#define _UTILS_H

#include <stdint.h>

#define TOGGLE_PIN(PIN)	(PIN ^= 1)

#define WRT_BITFLD(bitfield,position,value) ((bitfield) >> (position)) = \
	(value) & 1

#define RD_BITFLD(bitfield,position) (((bitfield) >> (position)) & 1)

uint16_t popcount(uint16_t x);

#endif
