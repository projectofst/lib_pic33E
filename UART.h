#ifndef _UART_H_
#define _UART_H_

#include "timing.h"
#include <xc.h>
#include <stdbool.h>
#include <stdint.h>

#define Fp                              FOSC/2
#define UART_BUFFER_LENGHT              70

typedef enum {_8_BIT_NO_PAR = 0, _8_BIT_EVEN_PAR = 1, _8_BIT_ODD_PAR = 2, _9_BIT_NO_PAR = 3} Data_Parity;


typedef struct{
    Data_Parity pdsel;                  /* number of bits in one data and type of parity*/
    unsigned int number_stop_bits;      /* 1/2 stop bits*/
    bool transmit;                      /* receive or transmite or both*/
    double BaudRate;                    /* define baudrate */
    unsigned int UxRX_idle_state :1;    /*idle state of the pin*/
    unsigned int UxTX_idle_state :1;    /*idle state of the pin*/
    unsigned int UxBRG :16;
    unsigned int BRGH :1;
}UART_parameters;

typedef struct{
    bool overflow;
    bool parity;
    bool framing;
}UART_errors;

int baudrate_generator(UART_parameters *parameters);
int config_uart1(UART_parameters *parameters);
int config_uart2(UART_parameters *parameters);
void enable_uart1(void);
void enable_uart2(void);
void disable_uart1(void);
void disable_uart2(void);
bool receive_uart1_buffer_empty(void);
bool receive_uart2_buffer_empty(void);
char get_FIFO_data(unsigned int uart_number);
UART_errors get_uart1_errors(void);
UART_errors get_uart2_errors(void);
char pop_uart1(void);
char pop_uart2(void);
bool uartRX1_empty(void);
bool uartRX2_empty(void);

#endif
